The goal of this section is to explain how the marketplace platform is used by providing a walkthrough of the functionalities available for the various users in this first release of the SecureIoT Marketplace. As more functionalities are implemented, this walkthrough section will be updated accordingly to reflect the full set of functionalities available in the marketplace. This way, this section will be also used as the base for the instruction manual of the marketplace.

The marketplace is available at the domain [marketplace.secureiot.eu](http://marketplace.secureiot.eu/). In section 2.1, we provide an overview of what is provided to the visitors of the marketplace, while in 2.2 and 2.3, we describe in detail how registered users can use the marketplace; first the potential customers of the services are presented, then the functionalities for users who want to offer their services through the marketplace is covered.



    1. Visitor Walkthrough 

When a user visits the marketplace.secureiot.eu, the landing page is shown which provides a short description of the marketplace, a short presentation of the available services and recently added services and popular choices, as depicted in Figure 1. This way, even the unregistered user is provided with a quick overview of the services available in the marketplace.

	



<p id="gdcalert1" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions0.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert2">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions0.png "image_tooltip")


**Figure 1: Landing page of the SecureIoT Marketplace**

The SecureIoT marketplace has been designed in such a way that not only provides the services but also provides relevant content that can assist building a strong user community focusing on the domain of IoT Security. To this end, the marketplace offers relevant static content, news, blog posts (see Figure 2), publications and material that can be useful for the users.



<p id="gdcalert2" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions1.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert3">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions1.png "image_tooltip")


**Figure 2: A blog post summary in SecureIoT Marketplace**

Figure 3 presents the login page of the platform.



<p id="gdcalert3" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions2.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert4">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions2.png "image_tooltip")


**Figure 3: Login page of the SecureIoT Marketplace**

If the user joins the platform for the first time, he/she can select to “Create Account” and be provided with the appropriate form (see Figure 4).



<p id="gdcalert4" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions3.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert5">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions3.png "image_tooltip")


**Figure 4: Registration page of the SecureIoT Marketplace**

In this form, the user is requested to provide a registration email, credentials, personal information such as name and also the affiliation, if any. As depicted in Figure 5 below, the user can select one of the existing organisations or create a new one. As SecureIoT marketplace is a Multi-Sided Platform, the registration of organizations is an important feature, as usually services are offered by organizations (e.g. SMEs or research organisations). 



<p id="gdcalert5" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions4.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert6">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions4.png "image_tooltip")
  

<p id="gdcalert6" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions5.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert7">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions5.png "image_tooltip")


**Figure 5: Registering of a user (selecting or creating an organization)**

After the registration, the user shall wait for acceptance by the platform administrator. By default, the newly registered users are provided with customer accounts (thus having the right to view services), while the administrator can change the account to a manager (the latter is capable of adding new services to the marketplace). In next iterations of the platform, the registration of users directly as managers will be implemented.



    2. Marketplace Member (Customer) Walkthrough 

When a user logs in to the platform with a customer account, an overview of the available content of the marketplace is provided, similarly to the visitor view. However, the customer is provided with an additional left side menu with options to see the services, and also his/her private message box and also his/her favourite services. 



<p id="gdcalert7" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions6.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert8">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions6.png "image_tooltip")


**Figure 6: Landing page of a registered user on SecureIoT Marketplace**

Then the user can see the list of available Security Solutions as depicted in Figure 7.



<p id="gdcalert8" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions7.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert9">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions7.png "image_tooltip")


**Figure 7: Grid view of Security Solutions in SecureIoT Marketplace**

Then, for each Security Solution, the user can see more details, as depicted in Figure 8.



<p id="gdcalert9" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions8.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert10">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions8.png "image_tooltip")


**Figure 8: Detailed view of a Security Solution in SecureIoT Marketplace**

The second category available for the user is the Business Services, such as consulting and business support services that can be offered by internal or external stakeholders (see Figure 9).



<p id="gdcalert10" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions9.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert11">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions9.png "image_tooltip")


**Figure 9: Grid view of Business Services in SecureIoT Marketplace**

A detailed view for the Business Services is also provided, as depicted in Figure 10.



<p id="gdcalert11" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions10.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert12">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions10.png "image_tooltip")


**Figure 10: Detailed view of a Business Service in SecureIoT Marketplace**

The third category offered is the Other Solutions, a category offering solutions created or offered by the SecureIoT consortium, as seen in Figure 11.



<p id="gdcalert12" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions11.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert13">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions11.png "image_tooltip")


**Figure 11: Grid view of Other Solutions Services in SecureIoT Marketplace**

A detailed view for the Other Solutions is also provided, as depicted in Figure 12.



<p id="gdcalert13" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions12.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert14">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions12.png "image_tooltip")


**Figure 12:  Detailed view of a Solution in SecureIoT Marketplace**

The customer is capable to rate the service independently of its category (Security Solutions, Business Services, Other Solutions), and to select it as a favourite. Rating is performed by clicking on the stars below service image, to select from 1 to 5 stars.  By selecting the heart button, a service is added as favorite. The list of all favorite services of the user is presented in Figure 13.



<p id="gdcalert14" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions13.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert15">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions13.png "image_tooltip")


**Figure 13: Member favorites on the Marketplace**

Finally, the members of the marketplace can communicate using the forum as well as the built-in messaging service. The goal of the built-in messaging service is to support the communication of the two different types of stakeholders; those providing a service and those using a service. This way, a customer who is interested in a service can communicate directly with its owner for requesting more information, asking questions or even mentioning issues, as depicted in Figure 14. 



<p id="gdcalert15" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions14.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert16">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions14.png "image_tooltip")


**Figure 14: Sending private messages through SecureIoT Marketplace**



    3. Manager Walkthrough 

When a user logs in to the platform with a manager account, a view similar to the customer one is provided, with the same three categories as described before. However, when selecting each of the categories, the manager accesses not only to customer view, but also to a dedicated view that can be used in order to create new services or to access the services that he/she offers. The customer view is depicted in Figure 15, while the manager view is depicted in Figure 16.



<p id="gdcalert16" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions15.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert17">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions15.png "image_tooltip")


**Figure 15: Security Solutions as shown in the user view of the Manager**



<p id="gdcalert17" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions16.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert18">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions16.png "image_tooltip")


**Figure 16: List of the Security Solutions as shown in the Manager view **

The manager can edit existing services (see Figure 17) or create new ones.



<p id="gdcalert18" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions17.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert19">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions17.png "image_tooltip")


**Figure 17: Editing a Security Solution**

The manager view for Business Services is depicted in Figure 18 below, where the specific user has not provided any service.



<p id="gdcalert19" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions18.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert20">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions18.png "image_tooltip")


**Figure 18: List of the Business Services as shown in the Manager view**

The manager can create new services through the dedicated form depicted in Figure 19.



<p id="gdcalert20" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions19.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert21">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions19.png "image_tooltip")


**Figure 19: Editing a Business Service**

The manager view for Other Solutions is depicted in Figure 20.



<p id="gdcalert21" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions20.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert22">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions20.png "image_tooltip")


**Figure 20: List of the Other Solutions as shown in the Manager view**

The manager can create new services through the dedicated form depicted in Figure 21.



<p id="gdcalert22" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions21.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert23">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions21.png "image_tooltip")


**Figure 21: Editing a Solution**



    4. Moderator Walkthrough 

The moderator is a role, responsible for the management of the content of the marketplace. Only people participating in the consortium can be provided with such an account. Currently, moderator accounts are provided to specific SingularLogic and Intrasoft members. In Figure 22, the landing page of the moderator is depicted; moderator can create new services or delete existing services, add or edit content, and add or edit target groups and service providers. 



<p id="gdcalert23" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions22.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert24">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions22.png "image_tooltip")


**Figure 22: Dashboard of the Moderator**

For each of the service categories (Security Solutions, Business Services, Other Solutions), the moderator can select, edit or delete the available services, from all service providers. The list view of the Security Solutions is provided in Figure 23.



<p id="gdcalert24" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions23.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert25">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions23.png "image_tooltip")


**Figure 23: Moderator view for Security Solution**

These services can be edited by the moderator by using the appropriate forms, as seen in Figure 24.



<p id="gdcalert25" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions24.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert26">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions24.png "image_tooltip")


**Figure 24: Editing a Security Solution (as moderator)**

In addition to the editing of the Security Solutions, the moderator is able to edit the values shown in the lists of the create and edit forms of Security Solutions, in specific the License Types (Figure 25) and Technology Readiness Level (Figure 26).



<p id="gdcalert26" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions25.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert27">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions25.png "image_tooltip")


**Figure 25: Managing Licence Types**



<p id="gdcalert27" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions26.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert28">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions26.png "image_tooltip")


**Figure 26: Managing Technology Readiness for the Servicesστ**

The moderator can also edit the Business Services, as seen in Figure 27 and Figure 28. 



<p id="gdcalert28" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions27.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert29">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions27.png "image_tooltip")


**Figure 27: Moderator view for Business Service**



<p id="gdcalert29" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions28.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert30">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions28.png "image_tooltip")


**Figure 28: Editing a Business Service (as moderator)**

Again, the moderator changes the values shown in the lists of the Business Service form; Price Ranges (see Figure 29) and the Training Type that is used for training services (see Figure 30).  



<p id="gdcalert30" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions29.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert31">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions29.png "image_tooltip")


**Figure 29: Managing pricing for Business Services**



<p id="gdcalert31" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions30.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert32">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions30.png "image_tooltip")


**Figure 30: Managing Training Types**

For managing the Other Solutions, Moderator is provided with the following view (Figure 31).



<p id="gdcalert32" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions31.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert33">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions31.png "image_tooltip")


**Figure 31: Moderator view for Other Solutions**

The edit form for Other Solutions is depicted in Figure 32.



<p id="gdcalert33" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions32.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert34">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions32.png "image_tooltip")


**Figure 32: Editing a Solution (as moderator)**

The moderator is also able to edit the values shown in the other lists, such as the Service Providers and the Target Groups, as well as to create, modify and delete static content, such as the publications. More features will be available in the next release of the marketplace.



    5. Administrator Walkthrough 

The final role presented in this deliverable is the role of administrator. This user is dedicated to the management of the core aspects of the marketplace and the management of the users, as seen in Figure 33.



<p id="gdcalert34" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions33.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert35">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions33.png "image_tooltip")


**Figure 33: Managing users**

As explained in D7.2, only approved users can register and use the platform. For this reason, the administrator is in charge of authorizing the registered users to actually use the marketplace.



<p id="gdcalert35" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions34.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert36">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions34.png "image_tooltip")


**Figure 34: Enabling and editing a user**

Also, the administrator can edit the organizations that the registered users belong to, through a dedicated page, as seen in Figure 35.



<p id="gdcalert36" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/instructions35.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert37">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/instructions35.png "image_tooltip")


**Figure 35: Managing Organizations**


<!-- Docs to Markdown version 1.0β17 -->
