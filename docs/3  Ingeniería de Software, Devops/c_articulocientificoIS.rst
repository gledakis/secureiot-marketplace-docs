.. _GeneralArticulosIS:

Scientific articles
####################

.. topic:: Scientific article,  Cloud Computing software engineering, DevOps:

   **Title**: Key Factors Impacting Cloud Computing Adoption

   **Source**: DOI: 10.1109/MC.2013.362 IEEE  Computer ( Volume: 46, Issue: 10, October 2013 )
                                              
   **Author**: Lorraine Morgan and Kieran Conboy

   **Date**: October 2013

   **Summary**: Findings from multiple cloud provider case studies identify 9 key factors and 15 subfactors affecting the adoption of cloud technology. These range from technological issues to broader organizational and environmental issues.

   **Target audience**: Startups/SMEs wanting to know the technological, organizational, and environmental factors that impact cloud services adoption.

   **Difficulty**: Low
######################

.. topic:: Scientific article,  Cloud Computing software engineering, DevOps:

   **Title**: Identification of SME Readiness to Implement Cloud Computing

   **Source**: DOI: 10.1109/ICCCSN.2012.6215757 / 2012 International Conference on Cloud Computing and Social Networking (ICCCSN)
                                              
   **Author**: Kridanto Surendro & Adiska Fardani

   **Date**: June 2012

   **Summary**: Cloud Computing allows the use of information technology based on the on-demand utility. This technology can provide benefits to small and medium enterprises with limited capital, human resources, and access to marketing network. A survey conducted on SMEs in the district of Coblong Bandung to dig up the IT needs and analyze their readiness to adopt cloud computing technologies. The survey results stated that SMEs’ respondents are more suitable to implement Software as a Service with public cloud deployment method. SMEs are ready to implement this technology, but requires appropriate training and role models that can be used as an example because their technology adoption characteristics that are late majority.

   **Target audience**: Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Scientific article,  Cloud Computing software engineering, DevOps:

   **Title**: Big Data Drives Cloud Adoption in Enterprise

   **Source**: DOI: 10.1109/MIC.2013.63 IEEE Internet Computing ( Volume: 17, Issue: 4, July-Aug. 2013 )
                                              
   **Author**: Huan Liu (Accenture Technology Labs)

   **Date**: June 2013

   **Summary**: The need to store, process, and analyze large amounts of data is finally driving enterprise customers to adopt cloud computing at scale. Understanding the economic drivers behind enterprise customers is key to designing next generation cloud services.

   **Target audience**: Startups/SMEs wanting to know important drivers to cloud adoption beyond cost reduction.

   **Difficulty**: Low
######################

.. topic:: Scientific article,  Cloud Computing software engineering, DevOps:

   **Title**: Elements of Cloud Adoption

   **Source**: DOI: 10.1109/MCC.2014.7 IEEE Cloud Computing ( Volume: 1, Issue: 1, May 2014 )
                                              
   **Author**: Samee U. Khan (North Dakota State University)

   **Date**: May 2014

   **Summary**: Sharing experiences in transitioning from traditional computing paradigms
to the cloud can provide a blueprint for organizations to gauge the depth
and breadth of cloud-enabled technologies.

   **Target audience**: Startups/SMEs.

   **Difficulty**: Low
######################

