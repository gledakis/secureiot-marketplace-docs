.. _GeneralVideosIS:

Tutorials
#########

.. topic:: Tutorials, Cloud Computing software engineering, DevOps:

   **Title**: What is DevOps? | DevOps Training - DevOps Introduction & Tools | DevOps Tutorial

   **Source**: https://youtu.be/3EyT1i0wYUY 
                                              
   **Author**: edureka!

   **Date**: 2016

   **Summary**: This DevOps tutorial takes you through what is DevOps all about and basic concepts of DevOps and DevOps Tools. This DevOps tutorial is ideal for beginners to get started with DevOps. Check our complete DevOps playlist here: http://goo.gl/O2vo13
DevOps Tutorial Blog Series: https://goo.gl/P0zAf

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Tutorials, Cloud Computing software engineering, DevOps:

   **Title**: Cloud Computing Adoption: Management Challenges and Opportunities

   **Source**: https://www.youtube.com/watch?v=gR_psSPqAnU
                                              
   **Author**: Julian Kudritzki and Andrew Reichman

   **Date**: Sept 2016

   **Summary**: Industry data from Uptime Institute and 451 Research evidence a rapid rate of cloud computing adoption for enterprise IT departments. Organizations weigh cloud benefits and risks, and also evaluate how cloud will impact their existing and future data center infrastructure investment. In this video, Uptime Institute COO Julian Kudritzki and Andrew Reichman, Research Director at 451 Research discuss the various aspects of how much risk, and how much reward, is on the table for companies considering a cloud transition.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Tutorials, Cloud Computing software engineering, DevOps:

   **Title**: On-Premises to Cloud: AWS Migration in 5 Super Easy Steps

   **Source**: https://serverguy.com/cloud/aws-migration/
                                              
   **Author**: Sahil Chugh

   **Date**: May 2018

   **Summary**: Five steps describing how to migrate applications and data to the AWS Cloud and detailing several strategies to do that. Wonderful article with lot of detailed information and examples.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Tutorials, Cloud Computing software engineering, DevOps:

   **Title**: On-Premises to Cloud: AWS Migration in 5 Super Easy Steps

   **Source**: https://github.com/google/go-cloud/tree/master/samples/tutorial
                                              
   **Author**: @zombiezen and @vangent, users of GitHub

   **Date**: Jul 2018

   **Summary**: With the premise that the best way to understand Go Cloud is to write some code and use it, the authors details how to build a command line application that uploads files to blob storage on both AWS and GCP. Blob storage stores binary data under a string key, and is one of the most frequently used cloud services.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Low
######################

.. topic:: Tutorials, Cloud Computing software engineering, DevOps:

   **Title**: Google Cloud tutorial: Get started with Google Cloud

   **Source**: https://www.infoworld.com/article/3267929/cloud-computing/google-cloud-tutorial-get-started-with-google-cloud.html
                                              
   **Author**: Peter Wayner

   **Date**: Apr 2018

   **Summary**: From virtual machines and Kubernetes clusters to serverless functions and machine learning APIs — how to navigate Google’s cloud riches.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Low
######################

.. topic:: Tutorials, Cloud Computing software engineering, DevOps:

   **Title**: Guide to planning your cloud adoption strategy

   **Source**: https://docs.newrelic.com/docs/using-new-relic/welcome-new-relic/plan-your-cloud-adoption-strategy/guide-planning-your-cloud-adoption-strategy
                                              
   **Author**: New Relic Inc.

   **Date**: Apr 2018

   **Summary**: Follow these guides as your roadmap through each phase of your cloud adoption journey: from planning your migration, to migrating your applications to the cloud, and, finally, to running your applications in the cloud successfully.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Low
######################