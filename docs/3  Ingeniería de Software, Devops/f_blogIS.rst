.. _GeneralBlogsIS:

Blogs
#####

.. topic:: Blogs, Cloud Computing software engineering, DevOps:

   **Title**: 4 things you need to know about managing complex hybrid clouds

   **Source**: https://techbeacon.com/4-things-you-need-know-about-managing-complex-hybrid-clouds
                 
   **Author**: David Linthicum

   **Date**: 23 January de 2018

   **Summary**: You need to know about managing complex hybrid clouds

   **Target audience**: Startups/SMEs 

   **Dificultad**: Medium
#########################

.. topic:: Blogs, Cloud Computing software engineering, DevOps:

   **Title**: How to choose: Open-source vs.commercial cloud management tools

   **Source**: https://techbeacon.com/how-choose-open-source-vs-commercial-cloud-management-tools
                 
   **Author**: David Linthicum

   **Date**: 5 December de 2017

   **Summary**: With so many open-source and commercial tools available for cloud management, how do you, as an IT operations management professional, decide which is the best fit for your team's needs? Here's a quick rundown of the general strengths and weaknesses of open-source versus commercial tool options, and when it makes sense to have one, the other, or a mix.

You need a plan for where free and open-source tools fit into your overall cloud management strategy, and where you should consider commercial options to complete the picture and meet your overall needs.

   **Target audience**: Startups/SMEs

   **Dificultad**: Low
#########################

.. topic:: Blogs, Cloud Computing software engineering, DevOps:

   **Title**: Infoworld — Cloud Computing

   **Source**: https://www.infoworld.com/blog/cloud-computing/
                 
   **Author**: David Linthicum

   **Date**: Sept 2018

   **Summary**: Infoworld’s outstanding cloud computing blog is written by David Linthicum, a consultant at Cloud Technology Partners and a sought-after industry expert and thought leader. His Infoworld blog is exclusively devoted to cloud computing and updated frequently. David’s recent blog ‘Featuritis’ Could Lead You to the Wrong Cloud recommends that enterprises concentrate on strategy and not features when making cloud service choices.

   **Target audience**: Startups/SMEs

   **Difficulty**: Low
######################

.. topic:: Blogs, Cloud Computing software engineering, DevOps:

   **Title**: All Things Distributed

   **Source**: https://www.allthingsdistributed.com/
                 
   **Author**: Werner Vogels

   **Date**: Sept 2018

   **Summary**: All Things Distributed is written by the world-famous Amazon CTO Werner Vogels. His blog is a must-read for anyone who uses AWS. He publishes sophisticated posts about specific AWS services and keeps his readers up-to-date on the latest AWS news. Recent blog posts include: Accelerating Data: Faster and More Scalable ElastiCache for Redis and New Ways to Discover and Use Alexa Skills.

   **Target audience**: Startups/SMEs

   **Difficulty**: Low
######################

.. topic:: Blogs, Cloud Computing software engineering, DevOps:

   **Title**: Compare the Cloud

   **Source**: https://www.comparethecloud.net/
                 
   **Author**: Several authors

   **Date**: Sept 2018

   **Summary**: All Things Distributed is written by the world-famous Amazon CTO Werner Vogels. His blog is a must-read for anyone who uses AWS. He publishes sophisticated posts about specific AWS services and keeps his readers up-to-date on the latest AWS news. Recent blog posts include: Accelerating Data: Faster and More Scalable ElastiCache for Redis and New Ways to Discover and Use Alexa Skills.

   **Target audience**: Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Blogs, Cloud Computing software engineering, DevOps:

   **Title**: Google Cloud Platform Blog

   **Source**: https://cloudplatform.googleblog.com/
                 
   **Author**: Google and Cloud experts

   **Date**: Sept 2018

   **Summary**: Google Cloud Platform’s blog contains hundreds of articles written by Google cloud experts, and actually dates back to 2008. This vast blog discusses the products, customers, and technical features of Google’s cloud solution, while articles can range from product blurbs to extremely detailed technical explanations. A recent post, Evaluating Cloud SQL Second Generation for Your Mobile Game, describes how Google’s structured query language can be applied to the special needs of game development.

   **Target audience**: Startups/SMEs

   **Difficulty**: Low
######################

.. topic:: Blogs, Cloud Computing software engineering, DevOps:

   **Title**: Prudent Cloud

   **Source**: http://www.prudentcloud.com/
                 
   **Author**: Subraya Mallya

   **Date**: Sept 2018

   **Summary**: The story behind why this blog was started helps to explain why it is a great blog for any cloud tech enthusiast and why Subraya is in zsah’s top 50 Cloud Bloggers. Subraya writes ‘Back when I was at a large company, I had the opportunity to interact with a large set of customers. During my interactions, it became apparent that there was a wide gap in the way we (technologists) thought about how technology gets used and the way our customers thought technology should work. So, I started PrudentCloud to share my thoughts and ideas on how these gaps can be bridged.

   **Target audience**: Startups/SMEs

   **Difficulty**: Low
######################

.. topic:: Blogs, Cloud Computing software engineering, DevOps:

   **Title**: ClearSky

   **Source**: https://www.clearskydata.com/blog/author/ellen-rubin
                 
   **Author**: Ellen Rubin

   **Date**: Sept 2018

   **Summary**: Ellen’s experience as an entrepreneur is what gives this ‘business and the cloud’ blog an edge. She writes from a place of experience and as she has a proven track record in leading strategy, market positioning and go-to-market for fast-growing companies, you can trust her words. Ellen writes blogs that are everyday user-friendly, making this blog a great destination for a start-up or a business at the start of their cloud journey.

   **Target audience**: Startups/SMEs

   **Difficulty**: Low
######################

.. topic:: Blogs, Cloud Computing software engineering, DevOps:

   **Title**: Right Scale

   **Source**: https://www.rightscale.com/blog/users/kim-weins
                 
   **Author**: Kim Weins

   **Date**: Sept 2018

   **Summary**: Kim’s blog is another blog targeted at every day users. She has the right balance of technical and commercial knowledge making this blog a well-balanced finished piece. She is currently the Vice President of Marketing for RightScale but obtained a B.S. in engineering from Duke University. Her writing often has a financial focus; comparing pricing and questioning spend.

   **Target audience**: Startups/SMEs

   **Difficulty**: Low
######################


