.. _GeneralArticulosDIS:

Magazine articles
#################

.. topic:: Magazine articles, Cloud Computing software engineering, DevOps:

   **Title**: Dos and don'ts: 9 effective best practices for DevOps in the cloud 

   **Source**: https://techbeacon.com/dos-donts-9-effective-best-practices-devops-cloud
                                              
   **Author**: David Linthicum

   **Date**: 

   **Summary**: DevOps and cloud computing are joined at the hip. Why? DevOps is about streamlining development so user requirements can quickly make it into application production, while the cloud offers automated provisioning and scaling to accommodate application changes.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Magazine articles, Cloud Computing software engineering, DevOps:

   **Title**: The Relationship Between The Cloud And DevOps

   **Source**: https://www.forbes.com/sites/forbestechcouncil/2017/07/21/the-relationship-between-the-cloud-and-devops/#4207c8822957
                                     
   **Author**: Aater Suleman

   **Date**: 2017

   **Summary**: Most companies understand that if they want to increase their competitiveness in today’s swiftly changing world, they can’t ignore digital transformation. DevOps and cloud computing are oft-touted as vital ways companies can achieve this needed transformation, though the relationship between the two is often confusing, as DevOps is about process and process improvement whereas cloud computing is about technology and services. Not mutually exclusive, it’s important to understand how the cloud and DevOps work together to help businesses achieve their transformation goals.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################