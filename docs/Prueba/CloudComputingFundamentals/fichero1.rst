############################
Cloud Computing Fundamentals
############################
=========
Tutorials
=========

   **Title**: Cloud Computing Services Models - IaaS PaaS SaaS Explained

   **Source**: https://youtu.be/36zducUX16w
                                              
   **Author**: Ecourse Review


   **Date**: April 2017

   **Summary**: The Three Delivery Models: Cloud computing provides different services based on three delivery configurations.  When they are arranged in a pyramid structure, they are in the order of SaaS, PaaS, and IaaS.The Three Services:

SaaS - Software as a Service 

This service provides on-demand pay per use of the application software for users and is independent of a platform. You do not have to install software on your computer, unlike a license paid program.  Cloud runs a single occurrence of the software, making it available for multiple end-users allowing the service to be cheap.  All the computing resources that are responsible for delivering SaaS are totally managed by the vendor.  The service is accessible through a web browser or lightweight client applications.

End customers use SaaS regularly. The most popular SaaS providers offer the following products and services:

Google Ecosystem including Gmail, Google Docs, Google Drive, Microsoft Office 365, and SalesForce.
 
PaaS - Platform as a Service

This service is mostly a development environment that is made up of a programming language execution environment, an operating system, web server, and database.  It provides an environment where users can build, compile, and run their program without worrying about an hidden infrastructure.  You manage the data and application resources.  All the other resources are managed by the vendor.  This is the realm for developers.  PaaS providers offer the following products and services:

Amazon Web services, Elastic Beanstalk, Google App Engine, Windows Azure, Heroku, and Force.com

IaaS - Infrastructure as a Service

This service provides the architecture and infrastructure.  It provides all computing resources but in a virtual environment so multiple users can have access. The resources include data storage, virtualization, servers, and networking.  Most vendors are responsible for managing them.  If you use this service, you are responsible for handling other resources including applications, data, runtime, and middleware.  This is mostly for SysAdmins.  IaaS providers offer the following products and services:


   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: Introduction to Cloud | Cloud Computing Tutorial for Beginners

   **Source**: https://youtu.be/usYySG1nbfI
                                              
   **Author**: Edureka!

   **Date**: April 2017

   **Summary**: This Edureka video on "Introduction To Cloud” will introduce you to basics of cloud computing and talk about different types of Cloud provides and its Service models. Following is the list of content covered in this session:

1. What is Cloud?
2. Uses of Cloud
3. Service Models
4. Deployment Models
5. Cloud Providers
6. Cloud Demo - AWS, Google Cloud, Azure

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: How It Works: Cloud Microservices

   **Source**: https://youtu.be/Pvbr5d2mIZs
                                              
   **Author**: IBM Think Academy

   **Date**: October 2019

   **Summary**: Microservices are an important piece of the new approach to cloud — many tiny pieces, in fact. But how do they all work together?  Find out in this video from Think Academy. For more information on IBM Cloud, please visit: http://www.ibm.com/cloud

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: Difference Between APIs, Services and Microservices?

   **Source**: https://youtu.be/qGFRbOq4fmQ
                                              
   **Author**: IBM Cloud

   **Date**: April 2017

   **Summary**: What's the difference between APIs, services, and microservices? Watch now and learn more here: ibm.co/2o1paz1

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: Learn All About Microservices — Microservices Architecture Example:

   **Source**: https://dzone.com/articles/microservices-tutorial-learn-all-about-microservic   
                                           
   **Author**: DZone

   **Date**: May 2018

   **Summary**: This microservices tutorial is the third part in this microservices blog series. I hope that you have read my previous blog, What is Microservices, which explains the architecture, compares microservices with monoliths and SOA, and explores when to use microservices with the help of use cases.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

=================
Magazine articles
=================

   **Title**: A Cloud You Can Trust

   **Source**: https://spectrum.ieee.org/computing/hardware/the-cloud-is-the-computer
                                              
   **Author**: Christian Cachin and Matthias Schunterterm

   **Date**: November 2011

   **Summary**: This past April, Amazon’s Elastic Compute Cloud service crashed during a system upgrade, knocking customers’ websites off-line for anywhere from several hours to several days. That same month, hackers broke into the Sony PlayStation Network, exposing the personal information of 77 million people around the world. And in June a software glitch at cloud-storage provider Dropbox temporarily allowed visitors to log in to any of its 25 million customers’ accounts using any password—or none at all. As a company blogger drily noted: “This should never have happened.”

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: Pros and Cons of Cloud Computing Technology 

   **Source**: https://www.ijsr.net/archive/v5i7/ART2016314.pdf
                                              
   **Author**: Sandeep Mukherji and Shashwat Srivastava

   **Date**: July de 2016

   **Summary**: Cloud computing technology today is the most enchanting technology that has become something for every business users or plans to move to. There are different types of cloud beneficial for the different users of the World, for the simple public cloud is most suitable, for the corporate world private cloud raise its hand and for the other users who are not completely corporate nor public hybrid cloud is chosen, all these three categories make all the uses complete for the world. Cloud computing is a boon for the business solution that every enterprise wants to adopt it. But having lots of pros in cloud computing, there are some cons also. This Paper is trying to put some light on those areas. 

   **Target audience**: Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: The Cloud Is The Computer

   **Source**: https://spectrum.ieee.org/computing/hardware/the-cloud-is-the-computer
                                         
   **Author**: Paul McFedries

   **Date**: Aug 2018

   **Summary**: In the past few years we've seen Sun's slogan morph from perplexing to prophetic. As we do more and more online, we see that the network--that is, the Internet--is now an extension of our computers, to say the least. Particularly with wireless technologies, we see that a big chunk of our computing lives now sits out there in that haze of data and connections known as the cloud . In fact, we're on the verge of cloud computing , in which not just our data but even our software resides within the cloud, and we access everything not only through our PCs but also cloud-friendly devices, such as smart phones, PDAs, computing appliances, gaming consoles, even cars.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: Escape From the Data Center: The Promise of Peer-to-Peer Cloud Computing

   **Source**: https://spectrum.ieee.org/computing/networks/escape-from-the-data-center-the-promise-of-peertopeer-cloud-computing
                                         
   **Author**: Ozalp Babaoglu and Moreno Marzolla

   **Date**: Sep 2014

   **Summary**: Today, cloud computing takes place in giant server farms owned by the likes of Amazon, Google, or Microsoft—but it doesn’t have to.Not long ago, any start-up hoping to create the next big thing on the Internet had to invest sizable amounts of money in computing hardware, network connectivity, real estate to house the equipment, and technical personnel to keep everything working 24/7. The inevitable delays in getting all this funded, designed, and set up could easily erase any competitive edge the company might have had at the outset.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: A Comprehensive Guide to Secure Cloud Computing.ISBN:0470589876 9780470589878

   **Source**: https://www.amazon.es/Cloud-Security-Comprehensive-Secure-Computing/dp/0470589876
                                         
   **Author**: Ronald L. KrutzRussell Dean Vines

   **Date**: Sep 2014

   **Summary**: Well-known security experts decipher the most challenging aspect of cloud computing-security Cloud computing allows for both large and small organizations to have the opportunity to use Internet-based services so that they can reduce start-up costs, lower capital expenditures, use services on a pay-as you-use basis, access applications only as needed, and quickly reduce or increase capacities. However, these benefits are accompanied by a myriad of security issues, and this valuable book tackles the most common security challenges that cloud computing faces. The authors offer you years of unparalleled expertise and knowledge as they discuss the extremely challenging topics of data ownership, privacy protections, data mobility, quality of service and service levels, bandwidth costs, data protection, and support. As the most current and complete guide to helping you find your way through a maze of security minefields, this book is mandatory reading if you are involved in any aspect of cloud computing. Coverage Includes: Cloud Computing Fundamentals Cloud Computing Architecture Cloud Computing Software Security Fundamentals Cloud Computing Risks Issues Cloud Computing Security Challenges Cloud Computing Security Architecture Cloud Computing Life Cycle Issues Useful Next Steps and Approaches).

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

===================
Scientific articles
===================

   **Title**: Security Problems of Platform-as-a-Service (PaaS) Clouds and Practical Solutions to the Problems 

   **Source**: https://www.computer.org/csdl/proceedings/srds/2012/2397/00/4784a463.pdf
                                              
   **Author**: Mehmet Tahir Sandıkkaya and Ali Emre Harmancı 

   **Date**: Noviembre 2012

   **Summary**: Cloud computing is a promising approach for the efficient use of computational resources. It delivers computing as a service rather than a product for a fraction of the cost. However, security concerns prevent many individuals and organizations from using clouds despite its cost effectiveness. Resolving security problems of clouds may alleviate concerns and increase cloud usage; in consequence, it may decrease overall costs spent for the computational devices and infrastructures. This paper particu- larly focuses on the Platform-as-a-Service (PaaS) clouds. Security of PaaS clouds is considered from multiple perspectives including access control, privacy and service continuity while protecting both the service provider and the user. Security problems of PaaS clouds are explored and classified. Countermeasures are proposed and discussed. The achieved solutions are intended to be the rationales for future PaaS designs and implementations.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: Performance of Cloud Based Solutions. The Impact of Public Cloud, Private Cloud and Hybrid Cloud

   **Source**: https://www.grin.com/document/294956?lang=es
                                              
   **Author**: Mehmet Tahir Sandıkkaya and Ali Emre Harmancı 

   **Date**: Noviembre 2014

   **Summary**: The cloud paradigm introduces a change in visualization of system and data owned by an enterprise. Further, the on service-based sharing of resources such as storage, hardware and applications which are delivered with cloud computing in a total different way has facilitated coherence of the resources and economies of scale through its pay-per-use business model. It is no longer a collection of devices on a physical location and run a particular software program with all the needed data and resources present at a physical location but instead is a system which is geographically distributed with consideration on both application and data. But even when the development of distributed cloud architectures and services are all dealing with the same issues of scalability, elasticity over demand, broad network access, usage measurement, security aspects such as authorization and authentication, and many other concepts related to multitenant services in order to serve a high number of concurrent users over the internet, is it the main goal for companies to find the right solution for their requierments. The right solution can be a public, a private or a hybrid cloud and although the issues are very similar in any of these solutions, it depends further on the degree of potency of one or some issues which are related to different kind of industries and organisations to evaluate the right cloud based approach for a particular company. This means we have to agree that the evolution of a new paradigm requires adaptation in usage patterns and associated functional areas to fully benefit from the paradigm shift.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: Security Issues: Public vs Private vs Hybrid Cloud Computing 

   **Source**: http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.245.1453&rep=rep1&type=pdf
                                              
   **Author**: R.Balasubramanian and M.Aramudhan

   **Date**: Octubre 2012

   **Summary**:Cloud computing appears as a new paradigm and its main objective is to provide secure, quick, convenient data storage and net computing service. Even though cloud computing effectively reduces the cost and maintenance of IT industry security issues plays a very important role. More and more IT companies are shifting to cloud based service like Private, Public and Hybrid cloud computing. But at the same time they are concerned about security issues. In this paper much attention is given to Public, Private and Hybrid cloud computing issues., as more business today utilize cloud services and architectures more threats and concerns arise. An analysis of comparative benefits of different styles of cloud computing by using SPSS is also discussed here. 

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: Survey on Microservice Architecture - Security, Privacy and Standardization on Cloud Computing Environment  

   **Source**: http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.245.1453&rep=rep1&type=pdf
                                              
   **Author**: Washington Henrique Carvalho Almeida, Luciano de Aguiar Monteiro, Raphael Rodrigues Hazin, Anderson Cavalcanti de Lima and Felipe Silva Ferraz

   **Date**: Octubre 2017

   **Summary**:Microservices have been adopted as a natural solution for the replacement of monolithic systems. Some technologies and standards have been adopted for the development of microservices in the cloud environment; API and REST have been adopted on a large scale for their implementation. The purpose of the present work is to carry out a bibliographic survey on the microservice architecture focusing mainly on security, privacy and standardization aspects on cloud computing environments. This paper presents a bundle of elements that must be considered for the construction of solutions based on microservices.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: SMicroservices Architecture based Cloudware Deployment Platform for Service Computing  

   **Source**: https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7473049
                                              
   **Author**: Dong Guo, Wei Wang , Guosun Zeng, Zerong Wei

   **Date**: Octubre 2017

   **Summary**: With the rising of Cloud computing, evolution have occurred not only in datacenter, but also in software development, deployment, maintain and usage. How to build cloud platform for traditional software, and how to deliver cloud service to users are central research fields which will have a huge impact. In recent years, the development of microservice and container technology make software paradigm evolve towards Cloudware in cloud environment. Cloudware, which is based on service and supported by cloud platform, is an important method to cloudalize traditional software. It is also a significant way for software development, deployment, maintenance and usage in future cloud environment. Furthermore, it creates a completely new thought for software in cloud platform. In this paper, we proposed a new Cloudware PaaS platform based on microservice architecture and light weighted container technology. We can directly deploy traditional software which provides services to users by browser in this platform without any modification. By utilizing the microservice architecture, this platform has the characteristics of scalability, auto-deployment, disaster recovery and elastic configuration. 

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

=======
Courses
=======

   **Title**: Introduction cloud computing

   **Source**: https://www.edx.org/es/course/introduction-cloud-computing-microsoft-cloud200x
                 
   **Author**: Microsoft

   **Date**: 2018

   **Summary**: Cloud computing, or “the cloud”, has gone from a leading trend in IT to mainstream consciousness and wide adoption.

This self-paced course introduces cloud computing concepts where you’ll explore the basics of cloud services and cloud deployment models. 

You’ll become acquainted with commonly used industry terms, typical business scenarios and applications for the cloud, and benefits and limitations inherent in the new paradigm that is the cloud.

This course will help prepare you for more advanced courses in Windows Server-based cloud and datacenter administration.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

=====
Blogs
=====

   **Title**: Blog Cloud Computing  David Linthicum

   **Source**: https://www.infoworld.com/blog/cloud-computing/
                 
   **Author**: David Linthicum, Cloud Computing

   **Date**: 2015

   **Summary**: David Linthicum, the CTO and founder of Blue Mountain Labs, is widely recognized as a thought leader in the cloud computing industry -- and with good reason. He travels to deliver keynote speeches on cloud, has contributed to or authored 13 books, and writes the Cloud Computing blog. With provocative titles such as "Shocker: Government agency drafts sensible cloud computing strategy," "Wozniak is wrong about cloud computing" and "Wake up, IT: Even CFOs see value in the cloud," Linthicum isn't afraid to state his opinion -- and with his extensive cloud background, everyone should take note.Check out Linthicum's monthly column on SearchCloudComputing.com, in which he discusses topics that range from the cloud API wars to cloud portability and interoperability struggles.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

   **Title**: Cloud Tech

   **Source**: https://www.cloudcomputing-news.net
                 
   **Author**: Cloud Tech

   **Date**: 2016

   **Summary**: CloudTech is a leading blog and news site that is dedicated to cloud computing strategy and technology. With authors including IBM’s Sebastian Krause, Cloudonomics author Joe Weinman, and Ian Moyse from the Cloud Industry Forum, CloudTech has hundreds of blogs about numerous cloud-related topics and reaches over 320,000 cloud computing professionals. A recent post How the Financial Services Industry Is Slowly Waking Up to Cloud Computing by Rahul Singh of HCL Technologies provides an interesting analysis of how banks can overcome the barriers to cloud migration.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

   **Title**: The best page with the 25 best blogs in the world of cloud computing

   **Source**: https://www.cloudendure.com/blog/top-25-must-read-cloud-computing-blogs/
                 
   **Author**: Cloudendure

   **Date**: 2018

   **Summary**: In this page you will find the 25 most relevant blogs in the world about cloud computing.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: Top five must-read cloud computing blogs

   **Source**: https://www.cloudendure.com/blog/top-25-must-read-cloud-computing-blogs/
                 
   **Author**: Werner Vogels, All Things Distributed, Randy Bias, Cloudscaling ,Adrian Cockcroft, Adrian Cockcroft's, Lori MacVittie, Two Different Socks, David Linthicum, Cloud Computing

   **Date**: 2015

   **Summary**: Werner Vogels, All Things Distributed: These top bloggers are in no particular order, but kicking off the list with the CTO of what's arguably cloud computing's king vendor, Amazon Web Services, should come as no surprise. Werner Vogels' blog, All Things Distributed, is not strictly about cloud computing, but it does give insight into AWS' strategy and vision, which should be of interest to any cloud pro. With some personality, regular updates and blogs jam-packed with information, Vogels proves he's one to beat in the blogging arena. Randy Bias, Cloudscaling blog: Randy Bias has a lengthy resume. Featured in publications as prestigious as Forbes, The Economist and The Wall Street Journal, Bias has a lot to say about cloud computing. This founding CEO and CTO of Cloudscaling also regularly contributes, along with other cloud experts and pioneers, to Cloudscaling's blog. With posts on Amazon's secret to success, the open source cloud market and the rise of DevOps, Bias focuses on what he calls "the profound disruption" cloud computing causes. If anyone can help IT navigate this disrupted landscape, Bias is at the top of the list. Adrian Cockcroft, Adrian Cockcroft's blog: Though simply titled, Adrian Cockcroft's blog has been covering the complex world of cloud architectures and performance tools -- as well as his interest in cars and music -- since 2004. He may not update the blog as often as Bias or Vogels, but his monthly (or so) insights are always worth a read. As a cloud architect at Netflix, Adrian Cockcroft is keen on discussing how his company handles the cloud computing evolution and how that compares to other industry leaders and trends, tackling subjects like Platform as a Service's (PaaS) place in the market and the use of popular AWS products.Lori MacVittie, Two Different Socks: Recognized as one of the Top Women in Cloud by CloudNOW, a nonprofit consortium featuring female leaders in cloud computing, Lori MacVittie is already well-known as a cloud leader. MacVittie's blog, Two Different Socks, delves into cloud performance struggles, DevOps and other vital issues in the cloud industry, with the help of charts, infographics and fun images. As the senior technical marketing manager at F5 Networks, MacVittie still had time to co-author a book called The Cloud Security Rules. David Linthicum, Cloud Computing: David Linthicum, the CTO and founder of Blue Mountain Labs, is widely recognized as a thought leader in the cloud computing industry -- and with good reason. He travels to deliver keynote speeches on cloud, has contributed to or authored 13 books, and writes the Cloud Computing blog. With provocative titles such as "Shocker: Government agency drafts sensible cloud computing strategy," "Wozniak is wrong about cloud computing" and "Wake up, IT: Even CFOs see value in the cloud," Linthicum isn't afraid to state his opinion -- and with his extensive cloud background, everyone should take note.Check out Linthicum's monthly column on SearchCloudComputing.com, in which he discusses topics that range from the cloud API wars to cloud portability and interoperability struggles.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################
