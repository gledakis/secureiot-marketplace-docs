.. SecureIoT documentation master file, created by
   sphinx-quickstart on Tue Aug 26 14:19:49 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the online documentation of SecureIoT Marketplace!
=======================================================



What is SecureIoT?
------------

A framework that provides security for IoT deployments.

.. image:: 5UsageGuide/images/instructions0.png


What is this page?
------------
The goal of this page is to explain how the marketplace platform is
used by providing a walkthrough of the functionalities available for the
various users in this first release of the SecureIoT Marketplace. As
more functionalities are implemented, this walkthrough will be
updated accordingly to reflect the full set of functionalities available
in the marketplace. 

The marketplace is available at the domain
`marketplace.secureiot.eu <http://marketplace.secureiot.eu/>`__. 




Online Documentation Contents
=====================================================================


.. toctree::
   :maxdepth: 2
   :caption: Basic Usage Guide

   5UsageGuide/a_getting-started-with-secureiotmarketplace
   5UsageGuide/b_resources
  


.. toctree::
  :maxdepth: 2
  :caption: Support
  
  support
