========
Dashboard
========
Access to the platform is provided through the URL
http://unicorn-project.eu/, where the end user is able to view introductory information
regarding the set of services provided through the platform.

.. image:: assets/intro_1.png

From the dashboard, access is provided to the xxxx.
Free access is provided to the public part of the xxx, while in the rest parts registration is required.

Login to account
-----------------
.. image:: assets/subsol_cr_1.png

- Click on "LOGIN" button.

.. image:: assets/subsol_cr_2.png

- You have successfully "LOGIN" to your account.

.. image:: assets/subsol_cr_3.png

Edit account
-------------

.. image:: assets/subsol_ed_1.png

- Click on "Edit Profile" button.

.. image:: assets/subsol_ed_2.png

- Provide your changes and click on "Save" button.

.. image:: assets/subsol_ed_3.png

- A pop-up message confirms that you have successfully updated your account.

.. image:: assets/subsol_ed_4.png
