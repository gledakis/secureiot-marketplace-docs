############################
Software engineering, DevOps 
############################

*DevOps and the cloud are closely related. The vast majority of cloud development projects use DevOps, and the list will get longer and longer. The benefits of using DevOps with cloud projects are also being better defined. The following resources provide relevant information on the methods of software development process related to Devops and particularly for the case of Cloud Computing.*

=================
Magazine articles
=================

   **Title**: Dos and don'ts: 9 effective best practices for DevOps in the cloud 

   **Source**: https://techbeacon.com/dos-donts-9-effective-best-practices-devops-cloud
                                              
   **Author**: David Linthicum

   **Date**: May 2016

   **Summary**: DevOps and cloud computing are joined at the hip. Why? DevOps is about streamlining development so user requirements can quickly make it into application production, while the cloud offers automated provisioning and scaling to accommodate application changes.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: The Relationship Between The Cloud And DevOps

   **Source**: https://www.forbes.com/sites/forbestechcouncil/2017/07/21/the-relationship-between-the-cloud-and-devops/#4207c8822957
                                     
   **Author**: Aater Suleman

   **Date**: 2017

   **Summary**: Most companies understand that if they want to increase their competitiveness in today’s swiftly changing world, they can’t ignore digital transformation. DevOps and cloud computing are oft-touted as vital ways companies can achieve this needed transformation, though the relationship between the two is often confusing, as DevOps is about process and process improvement whereas cloud computing is about technology and services. Not mutually exclusive, it’s important to understand how the cloud and DevOps work together to help businesses achieve their transformation goals.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

===================
Scientific articles
===================

  **Title**: Key Factors Impacting Cloud Computing Adoption

   **Source**: DOI: 10.1109/MC.2013.362 IEEE  Computer ( Volume: 46, Issue: 10, October 2013 )
                                              
   **Author**: Lorraine Morgan and Kieran Conboy

   **Date**: October 2013

   **Summary**: Findings from multiple cloud provider case studies identify 9 key factors and 15 subfactors affecting the adoption of cloud technology. These range from technological issues to broader organizational and environmental issues.

   **Target audience**: Startups/SMEs wanting to know the technological, organizational, and environmental factors that impact cloud services adoption.

   **Difficulty**: Low
######################

   **Title**: Identification of SME Readiness to Implement Cloud Computing

   **Source**: DOI: 10.1109/ICCCSN.2012.6215757 / 2012 International Conference on Cloud Computing and Social Networking (ICCCSN)
                                              
   **Author**: Kridanto Surendro & Adiska Fardani

   **Date**: June 2012

   **Summary**: Cloud Computing allows the use of information technology based on the on-demand utility. This technology can provide benefits to small and medium enterprises with limited capital, human resources, and access to marketing network. A survey conducted on SMEs in the district of Coblong Bandung to dig up the IT needs and analyze their readiness to adopt cloud computing technologies. The survey results stated that SMEs’ respondents are more suitable to implement Software as a Service with public cloud deployment method. SMEs are ready to implement this technology, but requires appropriate training and role models that can be used as an example because their technology adoption characteristics that are late majority.

   **Target audience**: Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: Big Data Drives Cloud Adoption in Enterprise

   **Source**: DOI: 10.1109/MIC.2013.63 IEEE Internet Computing ( Volume: 17, Issue: 4, July-Aug. 2013 )
                                              
   **Author**: Huan Liu (Accenture Technology Labs)

   **Date**: June 2013

   **Summary**: The need to store, process, and analyze large amounts of data is finally driving enterprise customers to adopt cloud computing at scale. Understanding the economic drivers behind enterprise customers is key to designing next generation cloud services.

   **Target audience**: Startups/SMEs wanting to know important drivers to cloud adoption beyond cost reduction.

   **Difficulty**: Low
######################

   **Title**: Elements of Cloud Adoption

   **Source**: DOI: 10.1109/MCC.2014.7 IEEE Cloud Computing ( Volume: 1, Issue: 1, May 2014 )
                                              
   **Author**: Samee U. Khan (North Dakota State University)

   **Date**: May 2014

   **Summary**: Sharing experiences in transitioning from traditional computing paradigms
to the cloud can provide a blueprint for organizations to gauge the depth
and breadth of cloud-enabled technologies.

   **Target audience**: Startups/SMEs.

   **Difficulty**: Low
######################

=========
Tutorials
=========

   **Title**: What is DevOps? | DevOps Training - DevOps Introduction & Tools | DevOps Tutorial

   **Source**: https://youtu.be/3EyT1i0wYUY 
                                              
   **Author**: edureka!

   **Date**: 2016

   **Summary**: This DevOps tutorial takes you through what is DevOps all about and basic concepts of DevOps and DevOps Tools. This DevOps tutorial is ideal for beginners to get started with DevOps. Check our complete DevOps playlist here: http://goo.gl/O2vo13
DevOps Tutorial Blog Series: https://goo.gl/P0zAf

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: Cloud Computing Adoption: Management Challenges and Opportunities

   **Source**: https://www.youtube.com/watch?v=gR_psSPqAnU
                                              
   **Author**: Julian Kudritzki and Andrew Reichman

   **Date**: Sept 2016

   **Summary**: Industry data from Uptime Institute and 451 Research evidence a rapid rate of cloud computing adoption for enterprise IT departments. Organizations weigh cloud benefits and risks, and also evaluate how cloud will impact their existing and future data center infrastructure investment. In this video, Uptime Institute COO Julian Kudritzki and Andrew Reichman, Research Director at 451 Research discuss the various aspects of how much risk, and how much reward, is on the table for companies considering a cloud transition.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: On-Premises to Cloud: AWS Migration in 5 Super Easy Steps

   **Source**: https://serverguy.com/cloud/aws-migration/
                                              
   **Author**: Sahil Chugh

   **Date**: May 2018

   **Summary**: Five steps describing how to migrate applications and data to the AWS Cloud and detailing several strategies to do that. Wonderful article with lot of detailed information and examples.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: On-Premises to Cloud: AWS Migration in 5 Super Easy Steps

   **Source**: https://github.com/google/go-cloud/tree/master/samples/tutorial
                                              
   **Author**: @zombiezen and @vangent, users of GitHub

   **Date**: Jul 2018

   **Summary**: With the premise that the best way to understand Go Cloud is to write some code and use it, the authors details how to build a command line application that uploads files to blob storage on both AWS and GCP. Blob storage stores binary data under a string key, and is one of the most frequently used cloud services.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Low
######################

   **Title**: Google Cloud tutorial: Get started with Google Cloud

   **Source**: https://www.infoworld.com/article/3267929/cloud-computing/google-cloud-tutorial-get-started-with-google-cloud.html
                                              
   **Author**: Peter Wayner

   **Date**: Apr 2018

   **Summary**: From virtual machines and Kubernetes clusters to serverless functions and machine learning APIs — how to navigate Google’s cloud riches.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Low
######################

   **Title**: Guide to planning your cloud adoption strategy

   **Source**: https://docs.newrelic.com/docs/using-new-relic/welcome-new-relic/plan-your-cloud-adoption-strategy/guide-planning-your-cloud-adoption-strategy
                                              
   **Author**: New Relic Inc.

   **Date**: Apr 2018

   **Summary**: Follow these guides as your roadmap through each phase of your cloud adoption journey: from planning your migration, to migrating your applications to the cloud, and, finally, to running your applications in the cloud successfully.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Low
######################

=======
Courses
=======

   **Title**: DOCKER FUNDAMENTALS + ENTERPRISE DEVELOPERS COURSE BUNDLE

   **Source**: https://training.docker.com/instructor-led-training/docker-fundamentals-enterprise-developers
                                              
   **Author**: Docker

   **Date**: 2017

   **Summary**: As the follow-on to the Docker Fundamentals course, Docker for Enterprise Operations is a role-based course designed for an organization’s Development and DevOps teams to accelerate their Docker journey in the enterprise. The course covers in-depth core advanced features of Docker EE and best practices to apply these features at scale with enterprise workloads. It is highly recommended to complete the Docker Fundamentals course as a pre-requisite.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

   **Title**: General docker courses

   **Source**: https://training.docker.com/instructor-led-training
                                              
   **Author**: Docker

   **Date**: 2017

   **Summary**: Docker Fundamentals: (2 days, $1495) - https://success.docker.com/training/courses/docker-fundamentals

This is THE introductory Docker course to give your team the best foundation for enterprise grade Docker use-cases.

Docker for Enterprise Developers: (2 days, $1995) - https://success.docker.com/training/courses/docker-for-enterprise-developers

As the follow-on to our Docker Fundamentals course, Docker for Enterprise Developers is a role-based course designed for an organization’s Development and DevOps teams to accelerate their Docker journey in the enterprise.

Docker for Enterprise Operations:(2 Days, $1995) - https://success.docker.com/trainingcourses/docker-for-enterprise-operations

This course is the second level of Docker's core curriculum for the enterprise and is focused on the Docker Operator role in administration of Docker Enterprise Edition Advanced.

Docker Troubleshooting & Support : (2 Days, $1995) - https://success.docker.com/training/courses/docker-support-troubleshooting

The Docker Troubleshooting & Support course is a role-based course designed for an organization’s support teams to troubleshoot the variety of issues that arise in their Docker journey.

Docker Security: (1 Day, $995) - https://success.docker.com/training/courses/docker-security

This is the Docker Security course for your entire organization. Get everyone “on-the-same page” and working together to secure your Dockerized environment. This hands-on workshop style course will give your team an overview of important security features and best practices to protect your containerized services.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

=====
Blogs
=====

   **Title**: 4 things you need to know about managing complex hybrid clouds

   **Source**: https://techbeacon.com/4-things-you-need-know-about-managing-complex-hybrid-clouds
                 
   **Author**: David Linthicum

   **Date**: 23 January de 2018

   **Summary**: You need to know about managing complex hybrid clouds

   **Target audience**: Startups/SMEs 

   **Dificultad**: Medium
#########################

   **Title**: How to choose: Open-source vs.commercial cloud management tools

   **Source**: https://techbeacon.com/how-choose-open-source-vs-commercial-cloud-management-tools
                 
   **Author**: David Linthicum

   **Date**: 5 December de 2017

   **Summary**: With so many open-source and commercial tools available for cloud management, how do you, as an IT operations management professional, decide which is the best fit for your team's needs? Here's a quick rundown of the general strengths and weaknesses of open-source versus commercial tool options, and when it makes sense to have one, the other, or a mix.

You need a plan for where free and open-source tools fit into your overall cloud management strategy, and where you should consider commercial options to complete the picture and meet your overall needs.

   **Target audience**: Startups/SMEs

   **Dificultad**: Low
######################

   **Title**: Infoworld — Cloud Computing

   **Source**: https://www.infoworld.com/blog/cloud-computing/
                 
   **Author**: David Linthicum

   **Date**: Sept 2018

   **Summary**: Infoworld’s outstanding cloud computing blog is written by David Linthicum, a consultant at Cloud Technology Partners and a sought-after industry expert and thought leader. His Infoworld blog is exclusively devoted to cloud computing and updated frequently. David’s recent blog ‘Featuritis’ Could Lead You to the Wrong Cloud recommends that enterprises concentrate on strategy and not features when making cloud service choices.

   **Target audience**: Startups/SMEs

   **Difficulty**: Low
######################

   **Title**: All Things Distributed

   **Source**: https://www.allthingsdistributed.com/
                 
   **Author**: Werner Vogels

   **Date**: Sept 2018

   **Summary**: All Things Distributed is written by the world-famous Amazon CTO Werner Vogels. His blog is a must-read for anyone who uses AWS. He publishes sophisticated posts about specific AWS services and keeps his readers up-to-date on the latest AWS news. Recent blog posts include: Accelerating Data: Faster and More Scalable ElastiCache for Redis and New Ways to Discover and Use Alexa Skills.

   **Target audience**: Startups/SMEs

   **Difficulty**: Low
######################

   **Title**: Compare the Cloud

   **Source**: https://www.comparethecloud.net/
                 
   **Author**: Several authors

   **Date**: Sept 2018

   **Summary**: All Things Distributed is written by the world-famous Amazon CTO Werner Vogels. His blog is a must-read for anyone who uses AWS. He publishes sophisticated posts about specific AWS services and keeps his readers up-to-date on the latest AWS news. Recent blog posts include: Accelerating Data: Faster and More Scalable ElastiCache for Redis and New Ways to Discover and Use Alexa Skills.

   **Target audience**: Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: Google Cloud Platform Blog

   **Source**: https://cloudplatform.googleblog.com/
                 
   **Author**: Google and Cloud experts

   **Date**: Sept 2018

   **Summary**: Google Cloud Platform’s blog contains hundreds of articles written by Google cloud experts, and actually dates back to 2008. This vast blog discusses the products, customers, and technical features of Google’s cloud solution, while articles can range from product blurbs to extremely detailed technical explanations. A recent post, Evaluating Cloud SQL Second Generation for Your Mobile Game, describes how Google’s structured query language can be applied to the special needs of game development.

   **Target audience**: Startups/SMEs

   **Difficulty**: Low
######################

   **Title**: Prudent Cloud

   **Source**: http://www.prudentcloud.com/
                 
   **Author**: Subraya Mallya

   **Date**: Sept 2018

   **Summary**: The story behind why this blog was started helps to explain why it is a great blog for any cloud tech enthusiast and why Subraya is in zsah’s top 50 Cloud Bloggers. Subraya writes ‘Back when I was at a large company, I had the opportunity to interact with a large set of customers. During my interactions, it became apparent that there was a wide gap in the way we (technologists) thought about how technology gets used and the way our customers thought technology should work. So, I started PrudentCloud to share my thoughts and ideas on how these gaps can be bridged.

   **Target audience**: Startups/SMEs

   **Difficulty**: Low
######################

   **Title**: ClearSky

   **Source**: https://www.clearskydata.com/blog/author/ellen-rubin
                 
   **Author**: Ellen Rubin

   **Date**: Sept 2018

   **Summary**: Ellen’s experience as an entrepreneur is what gives this ‘business and the cloud’ blog an edge. She writes from a place of experience and as she has a proven track record in leading strategy, market positioning and go-to-market for fast-growing companies, you can trust her words. Ellen writes blogs that are everyday user-friendly, making this blog a great destination for a start-up or a business at the start of their cloud journey.

   **Target audience**: Startups/SMEs

   **Difficulty**: Low
######################

   **Title**: Right Scale

   **Source**: https://www.rightscale.com/blog/users/kim-weins
                 
   **Author**: Kim Weins

   **Date**: Sept 2018

   **Summary**: Kim’s blog is another blog targeted at every day users. She has the right balance of technical and commercial knowledge making this blog a well-balanced finished piece. She is currently the Vice President of Marketing for RightScale but obtained a B.S. in engineering from Duke University. Her writing often has a financial focus; comparing pricing and questioning spend.

   **Target audience**: Startups/SMEs

   **Difficulty**: Low
######################