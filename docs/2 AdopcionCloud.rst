.. _Cloud Adopcion:

************************************
Adoption of Cloud Computing paradigm
************************************

*Potential adopters of Cloud Computing technology perceive the adoption decision as very challenging. Cloud Computing adoption should be driven by change management, competence, maturity, among others. Cloud Computing is still in its stage of emergence, and there is still a lack of both knowledge and empirical evidence about which issues are the most significant for Cloud Computing adoption decisions. The following resources have been selected to give some light to these issues.*

* :ref:`GeneralArticulosDAdopcion`
* :ref:`GeneralArticulosCAdopcion`
* :ref:`GeneralVideosAdopcion`
* :ref:`GeneralCoursesAdopcion`
* :ref:`GeneralBlogsAdopcion`