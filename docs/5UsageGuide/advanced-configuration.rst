###############################################################
Advanced Options
###############################################################


.. note::
  This section is still under work

========
Overview
========
xxx

========
Features
========

* xxxx


===========
How to use?
===========
The elasticity policies can be defined both at the design time of the application
through the UNICORN docker-compose file, and during runtime via the service graph
of the application.

The following example shows a simple scale out policy for the **streaming_svc** service:
::

  scale_out_streaming_svc =
     WHEN
        average_requests_5m > 100
        AND
        average_cpu_5m   > 80
     THEN
        SCALE OUT  ( 1 service_streaming IN eu_zone WITH 30 SECONDS COOLDOWN)

The first part (**WHEN**) contains two conditions. These conditions specify that
the ``average_requests_5m`` must be greater than 100, while also the ``average_cpu_5m``
must exceed 80(%) in order to trigger the scaling action. The action (specified in **THEN** part),
designates that 1 more ``service_streaming`` service should be provisioned in ``eu_zone``.
Note that the ``WITH 30 SECONDS COOLDOWN`` is a configurable time period which is used to give time to the system to
provision/de-provision new resources and absorb any changes, in order to avoid false scaling alerts.


.. note::
  Here we should add screenshots from the Dashboard, showing how to add elasticity
  poicies.


=================
Elasticity Policy
=================

An Elasticity Policy is composed from a set of conditions and a scaling action.
Each condition contains an expression of an *analytic insight* (left hand-side)
and a number (right hand-side) which are operated by a binary operation (e.g., <,>,==).

*****************
Available Actions
*****************
Currently two actions are supported for horizontal scalability.
The ``SCALE OUT`` action, which provisions a new instance of a service and the
``SCALE IN`` action, that de-provisions an existing instance of a service.
