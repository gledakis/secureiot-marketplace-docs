###############################################################
Managing Components and Applications
###############################################################


.. note::
  This section is still under work

========
Overview
========
xxx


==================
Manage Componets
==================

- In UNICORN, applications are described as **microservices** composed by smaller **components**.

- A list of the existing components is provided.

.. image:: assets/compo_list.PNG

- Also, the user can create new components.

- For each component the name, the architecture and the way that the specific component scales have to be defined.

- Then the docker container image that is used for this component has to be defined.

  - Custom Docker registries are supported.

.. image:: assets/compo_part1.PNG

- Execution Requirements are defined, and custom health checks can be added to ensure that the service is deployed properly..

.. image:: assets/compo_part2.PNG

- The service of the component must use environmental variables that can be configured by the user.
- As an example we have a WordPress component;
   - Environmental variables as WORDPRESS_DB_USER and WORDPRESS_DB_PASSWORD can be used for the configuration of the component, and default values can be added
   - Adding @ and the component name (e.g: @MariaDB) means that the WordPress component will dynamically get the IP that the corresponding component will get once deployed.

.. image:: assets/compo_part3.PNG

- The interfaces exposed and the interfaces required by the service have to be defined.
   - A user can select one of the existing interfaces, like the TCP access through port 80, or define a new interface.
   - For the definition of the required interface, an existing exposed interface of another component has to be selected.

.. image:: assets/compo_part4.PNG

- Additional details like volumes, devices and labels for the component can be defined.

.. image:: assets/compo_part5.PNG

====================
Manage Applications
====================

- After the needed components have been defined, the user can proceed with the definition of the application.

- The application will be created through the help of a visual graph editor and then will be available for deployment.

.. image:: assets/applist.PNG

- At the visual editor, the application components are presented as the nodes of a graph, and the connection between the nodes is describing the interfaces between the services.

.. image:: assets/graph1.PNG

- Through the left side panel, the components can be retrieved and added to the editor.

.. image:: assets/graph2_details.PNG

- By selecting the required interface and dragging it to another node, the connection between the interfaces of the components can be done.

.. image:: assets/graph3_1.PNG

- This procedure is followed until all required interfaces have been connected in order to save a valid application graph.

.. image:: assets/graph3_3.PNG

.. image:: assets/graph3_12.PNG

=============================
Manage Applications instances
=============================

- Now the application can be instantiated and deployed to the cloud that user desires.

.. image:: assets/instance_create1.PNG

- By pressing "Proceed" the deployment starts.
- However, the user can also configure the application components before deployment.

.. image:: assets/instance_graph.PNG

- Prior to the deployment the user can activate the embedded Intrusion Prevention Mechanism (IPS) and Intrusion Detection Mechanism (IDS), by selecting the checkbox.

- The minimum and maximum amount of workers per node are defined in order to specify the scalability profile of the application.

.. image:: assets/instance_compo1.PNG

- The environmental variables of the application can also be configured prior to the deployment.

.. image:: assets/instance_compo2.PNG

- Deployment procedure needs few minutes to finish. The user is constantly informed by viewing the logs aggregated from all the nodes of the application.
   - The total deployment time depends on the cloud infrastructure selected, as the spawning of new VMs might take more time in some IaaS.
   - Total time is also affected by the network delays between the cloud infrastructure and the docker registry that is used to fetch the components container image.

.. image:: assets/deployment23.PNG

- When deployment finishes all noded turn green
   - On the instance list the application is shown as "DEPLOYED".
- Monitoring metrics are presented for each one of the application nodes

.. image:: assets/instance_deploymentlist_best.PNG
