###############################################################
Marketplace Member (Customer) Walkthrough 
###############################################################



When a user logs in to the platform with a customer account, an overview
of the available content of the marketplace is provided, similarly to
the visitor view. However, the customer is provided with an additional
left side menu with options to see the services, and also his/her
private message box and also his/her favourite services.

.. raw:: html

   <p id="gdcalert7" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions6.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions6.png
   :alt: image_tooltip

   alt\_text
**Figure 6: Landing page of a registered user on SecureIoT Marketplace**

Then the user can see the list of available Security Solutions as
depicted in Figure 7.

.. raw:: html

   <p id="gdcalert8" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions7.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions7.png
   :alt: image_tooltip

   alt\_text
**Figure 7: Grid view of Security Solutions in SecureIoT Marketplace**

Then, for each Security Solution, the user can see more details, as
depicted in Figure 8.

.. raw:: html

   <p id="gdcalert9" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions8.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions8.png
   :alt: image_tooltip

   alt\_text
**Figure 8: Detailed view of a Security Solution in SecureIoT
Marketplace**

The second category available for the user is the Business Services,
such as consulting and business support services that can be offered by
internal or external stakeholders (see Figure 9).

.. raw:: html

   <p id="gdcalert10" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions9.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions9.png
   :alt: image_tooltip

   alt\_text
**Figure 9: Grid view of Business Services in SecureIoT Marketplace**

A detailed view for the Business Services is also provided, as depicted
in Figure 10.

.. raw:: html

   <p id="gdcalert11" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions10.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions10.png
   :alt: image_tooltip

   alt\_text
**Figure 10: Detailed view of a Business Service in SecureIoT
Marketplace**

The third category offered is the Other Solutions, a category offering
solutions created or offered by the SecureIoT consortium, as seen in
Figure 11.

.. raw:: html

   <p id="gdcalert12" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions11.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions11.png
   :alt: image_tooltip

   alt\_text
**Figure 11: Grid view of Other Solutions Services in SecureIoT
Marketplace**

A detailed view for the Other Solutions is also provided, as depicted in
Figure 12.

.. raw:: html

   <p id="gdcalert13" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions12.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions12.png
   :alt: image_tooltip

   alt\_text
**Figure 12: Detailed view of a Solution in SecureIoT Marketplace**

The customer is capable to rate the service independently of its
category (Security Solutions, Business Services, Other Solutions), and
to select it as a favourite. Rating is performed by clicking on the
stars below service image, to select from 1 to 5 stars. By selecting the
heart button, a service is added as favorite. The list of all favorite
services of the user is presented in Figure 13.

.. raw:: html

   <p id="gdcalert14" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions13.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions13.png
   :alt: image_tooltip

   alt\_text
**Figure 13: Member favorites on the Marketplace**

Finally, the members of the marketplace can communicate using the forum
as well as the built-in messaging service. The goal of the built-in
messaging service is to support the communication of the two different
types of stakeholders; those providing a service and those using a
service. This way, a customer who is interested in a service can
communicate directly with its owner for requesting more information,
asking questions or even mentioning issues, as depicted in Figure 14.

.. raw:: html

   <p id="gdcalert15" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions14.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions14.png
   :alt: image_tooltip

   alt\_text
**Figure 14: Sending private messages through SecureIoT Marketplace**

::

    3. Manager Walkthrough 

When a user logs in to the platform with a manager account, a view
similar to the customer one is provided, with the same three categories
as described before. However, when selecting each of the categories, the
manager accesses not only to customer view, but also to a dedicated view
that can be used in order to create new services or to access the
services that he/she offers. The customer view is depicted in Figure 15,
while the manager view is depicted in Figure 16.

.. raw:: html

   <p id="gdcalert16" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions15.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions15.png
   :alt: image_tooltip

   alt\_text
**Figure 15: Security Solutions as shown in the user view of the
Manager**

.. raw:: html

   <p id="gdcalert17" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions16.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions16.png
   :alt: image_tooltip

   alt\_text
**Figure 16: List of the Security Solutions as shown in the Manager view
**

The manager can edit existing services (see Figure 17) or create new
ones.

.. raw:: html

   <p id="gdcalert18" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions17.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions17.png
   :alt: image_tooltip

   alt\_text
**Figure 17: Editing a Security Solution**

The manager view for Business Services is depicted in Figure 18 below,
where the specific user has not provided any service.

.. raw:: html

   <p id="gdcalert19" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions18.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions18.png
   :alt: image_tooltip

   alt\_text
**Figure 18: List of the Business Services as shown in the Manager
view**

The manager can create new services through the dedicated form depicted
in Figure 19.

.. raw:: html

   <p id="gdcalert20" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions19.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions19.png
   :alt: image_tooltip

   alt\_text
**Figure 19: Editing a Business Service**

The manager view for Other Solutions is depicted in Figure 20.

.. raw:: html

   <p id="gdcalert21" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions20.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions20.png
   :alt: image_tooltip

   alt\_text
**Figure 20: List of the Other Solutions as shown in the Manager view**

The manager can create new services through the dedicated form depicted
in Figure 21.

.. raw:: html

   <p id="gdcalert22" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions21.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions21.png
   :alt: image_tooltip

   alt\_text
**Figure 21: Editing a Solution**

::

    4. Moderator Walkthrough 

The moderator is a role, responsible for the management of the content
of the marketplace. Only people participating in the consortium can be
provided with such an account. Currently, moderator accounts are
provided to specific SingularLogic and Intrasoft members. In Figure 22,
the landing page of the moderator is depicted; moderator can create new
services or delete existing services, add or edit content, and add or
edit target groups and service providers.

.. raw:: html

   <p id="gdcalert23" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions22.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions22.png
   :alt: image_tooltip

   alt\_text
**Figure 22: Dashboard of the Moderator**

For each of the service categories (Security Solutions, Business
Services, Other Solutions), the moderator can select, edit or delete the
available services, from all service providers. The list view of the
Security Solutions is provided in Figure 23.

.. raw:: html

   <p id="gdcalert24" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions23.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions23.png
   :alt: image_tooltip

   alt\_text
**Figure 23: Moderator view for Security Solution**

These services can be edited by the moderator by using the appropriate
forms, as seen in Figure 24.

.. raw:: html

   <p id="gdcalert25" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions24.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions24.png
   :alt: image_tooltip

   alt\_text
**Figure 24: Editing a Security Solution (as moderator)**

In addition to the editing of the Security Solutions, the moderator is
able to edit the values shown in the lists of the create and edit forms
of Security Solutions, in specific the License Types (Figure 25) and
Technology Readiness Level (Figure 26).

.. raw:: html

   <p id="gdcalert26" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions25.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions25.png
   :alt: image_tooltip

   alt\_text
**Figure 25: Managing Licence Types**

.. raw:: html

   <p id="gdcalert27" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions26.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions26.png
   :alt: image_tooltip

   alt\_text
**Figure 26: Managing Technology Readiness for the Servicesστ**

The moderator can also edit the Business Services, as seen in Figure 27
and Figure 28.

.. raw:: html

   <p id="gdcalert28" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions27.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions27.png
   :alt: image_tooltip

   alt\_text
**Figure 27: Moderator view for Business Service**

.. raw:: html

   <p id="gdcalert29" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions28.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions28.png
   :alt: image_tooltip

   alt\_text
**Figure 28: Editing a Business Service (as moderator)**

Again, the moderator changes the values shown in the lists of the
Business Service form; Price Ranges (see Figure 29) and the Training
Type that is used for training services (see Figure 30).

.. raw:: html

   <p id="gdcalert30" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions29.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions29.png
   :alt: image_tooltip

   alt\_text
**Figure 29: Managing pricing for Business Services**

.. raw:: html

   <p id="gdcalert31" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions30.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions30.png
   :alt: image_tooltip

   alt\_text
**Figure 30: Managing Training Types**

For managing the Other Solutions, Moderator is provided with the
following view (Figure 31).

.. raw:: html

   <p id="gdcalert32" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions31.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions31.png
   :alt: image_tooltip

   alt\_text
**Figure 31: Moderator view for Other Solutions**

The edit form for Other Solutions is depicted in Figure 32.

.. raw:: html

   <p id="gdcalert33" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions32.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions32.png
   :alt: image_tooltip

   alt\_text
**Figure 32: Editing a Solution (as moderator)**

The moderator is also able to edit the values shown in the other lists,
such as the Service Providers and the Target Groups, as well as to
create, modify and delete static content, such as the publications. More
features will be available in the next release of the marketplace.

::

    5. Administrator Walkthrough 

The final role presented in this deliverable is the role of
administrator. This user is dedicated to the management of the core
aspects of the marketplace and the management of the users, as seen in
Figure 33.

.. raw:: html

   <p id="gdcalert34" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions33.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions33.png
   :alt: image_tooltip

   alt\_text
**Figure 33: Managing users**

As explained in D7.2, only approved users can register and use the
platform. For this reason, the administrator is in charge of authorizing
the registered users to actually use the marketplace.

.. raw:: html

   <p id="gdcalert35" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions34.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions34.png
   :alt: image_tooltip

   alt\_text
**Figure 34: Enabling and editing a user**

Also, the administrator can edit the organizations that the registered
users belong to, through a dedicated page, as seen in Figure 35.

.. raw:: html

   <p id="gdcalert36" >

>>>>> gd2md-html alert: inline image link here (to
images/instructions35.png). Store image on your image server and adjust
path/filename if necessary. (Back to top)(Next alert)>>>>>

.. raw:: html

   </p>

.. figure:: images/instructions35.png
   :alt: image_tooltip

   alt\_text
**Figure 35: Managing Organizations**

.. raw:: html

   <!-- Docs to Markdown version 1.0β17 -->


