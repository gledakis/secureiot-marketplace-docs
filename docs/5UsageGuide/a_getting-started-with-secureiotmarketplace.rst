
###############################################################
Basic information and registration instructions
###############################################################


=============================
    1. Visitor Walkthrough 
=============================

When a user visits the marketplace.secureiot.eu, the landing page is
shown which provides a short description of the marketplace, a short
presentation of the available services and recently added services and
popular choices, as depicted in Figure 1. This way, even the
unregistered user is provided with a quick overview of the services
available in the marketplace.


.. figure:: images/instructions0.png
**Figure 1: Landing page of the SecureIoT Marketplace**

The SecureIoT marketplace has been designed in such a way that not only
provides the services but also provides relevant content that can assist
building a strong user community focusing on the domain of IoT Security.
To this end, the marketplace offers relevant static content, news, blog
posts (see Figure 2), publications and material that can be useful for
the users.

.. figure:: images/instructions1.png
**Figure 2: A blog post summary in SecureIoT Marketplace**


=============================
    2. User Login 
=============================

Figure 3 presents the login page of the platform.

.. figure:: images/instructions2.png
**Figure 3: Login page of the SecureIoT Marketplace**


=============================
    3.User Registration 
=============================

If the user joins the platform for the first time, he/she can select to
“Create Account” and be provided with the appropriate form (see Figure
4).


.. figure:: images/instructions3.png
**Figure 4: Registration page of the SecureIoT Marketplace**

In this form, the user is requested to provide a registration email,
credentials, personal information such as name and also the affiliation,
if any. As depicted in Figure 5 below, the user can select one of the
existing organisations or create a new one.

.. figure:: images/instructions5.png
**Figure 5: Registering of a user (selecting an existing organization)**

To create a new organization, type the name, then click on the name and a form requesting more information appears. 

.. figure:: images/instructions4.png
**Figure 6: Registering of a user (creating a new organization)**

After the registration, the user shall receive an acceptance email by the platform. By default, the newly registered users are
provided with customer accounts (thus having the right to view services). For allowing your account to be a manager in order to add new services to the marketplace please contact us (email: info@secureiot.eu).


