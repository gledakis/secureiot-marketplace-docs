.. _GeneralVideos:

Tutorials
#########

.. topic:: Tutorials, Cloud Computing:

   **Title**: Cloud Computing Services Models - IaaS PaaS SaaS Explained

   **Source**: https://youtu.be/36zducUX16w
                                              
   **Author**: Ecourse Review


   **Date**: April 2017

   **Summary**: The Three Delivery Models: Cloud computing provides different services based on three delivery configurations.  When they are arranged in a pyramid structure, they are in the order of SaaS, PaaS, and IaaS.The Three Services:

SaaS - Software as a Service 

This service provides on-demand pay per use of the application software for users and is independent of a platform. You do not have to install software on your computer, unlike a license paid program.  Cloud runs a single occurrence of the software, making it available for multiple end-users allowing the service to be cheap.  All the computing resources that are responsible for delivering SaaS are totally managed by the vendor.  The service is accessible through a web browser or lightweight client applications.

End customers use SaaS regularly. The most popular SaaS providers offer the following products and services:

Google Ecosystem including Gmail, Google Docs, Google Drive, Microsoft Office 365, and SalesForce.
 
PaaS - Platform as a Service

This service is mostly a development environment that is made up of a programming language execution environment, an operating system, web server, and database.  It provides an environment where users can build, compile, and run their program without worrying about an hidden infrastructure.  You manage the data and application resources.  All the other resources are managed by the vendor.  This is the realm for developers.  PaaS providers offer the following products and services:

Amazon Web services, Elastic Beanstalk, Google App Engine, Windows Azure, Heroku, and Force.com

IaaS - Infrastructure as a Service

This service provides the architecture and infrastructure.  It provides all computing resources but in a virtual environment so multiple users can have access. The resources include data storage, virtualization, servers, and networking.  Most vendors are responsible for managing them.  If you use this service, you are responsible for handling other resources including applications, data, runtime, and middleware.  This is mostly for SysAdmins.  IaaS providers offer the following products and services:


   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Tutorials, Cloud Computing:

   **Title**: Introduction to Cloud | Cloud Computing Tutorial for Beginners

   **Source**: https://youtu.be/usYySG1nbfI
                                              
   **Author**: Edureka!

   **Date**: April 2017

   **Summary**: This Edureka video on "Introduction To Cloud” will introduce you to basics of cloud computing and talk about different types of Cloud provides and its Service models. Following is the list of content covered in this session:

1. What is Cloud?
2. Uses of Cloud
3. Service Models
4. Deployment Models
5. Cloud Providers
6. Cloud Demo - AWS, Google Cloud, Azure

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Tutorials, Cloud Computing:

   **Title**: How It Works: Cloud Microservices

   **Source**: https://youtu.be/Pvbr5d2mIZs
                                              
   **Author**: IBM Think Academy

   **Date**: October 2019

   **Summary**: Microservices are an important piece of the new approach to cloud — many tiny pieces, in fact. But how do they all work together?  Find out in this video from Think Academy. For more information on IBM Cloud, please visit: http://www.ibm.com/cloud

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Tutorials, Cloud Computing:

   **Title**: Difference Between APIs, Services and Microservices?

   **Source**: https://youtu.be/qGFRbOq4fmQ
                                              
   **Author**: IBM Cloud

   **Date**: April 2017

   **Summary**: What's the difference between APIs, services, and microservices? Watch now and learn more here: ibm.co/2o1paz1

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Tutorials, Cloud Computing:

   **Title**: Learn All About Microservices — Microservices Architecture Example:

   **Source**: https://dzone.com/articles/microservices-tutorial-learn-all-about-microservic   
                                           
   **Author**: DZone

   **Date**: May 2018

   **Summary**: This microservices tutorial is the third part in this microservices blog series. I hope that you have read my previous blog, What is Microservices, which explains the architecture, compares microservices with monoliths and SOA, and explores when to use microservices with the help of use cases.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

