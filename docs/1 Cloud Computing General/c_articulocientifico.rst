.. _GeneralArticulosC:

Scientific articles
####################

.. topic:: Scientific article, Cloud Computing:

   **Title**: Security Problems of Platform-as-a-Service (PaaS) Clouds and Practical Solutions to the Problems 

   **Source**: https://www.computer.org/csdl/proceedings/srds/2012/2397/00/4784a463.pdf
                                              
   **Author**: Mehmet Tahir Sandıkkaya and Ali Emre Harmancı 

   **Date**: Noviembre 2012

   **Summary**: Cloud computing is a promising approach for the efficient use of computational resources. It delivers computing as a service rather than a product for a fraction of the cost. However, security concerns prevent many individuals and organizations from using clouds despite its cost effectiveness. Resolving security problems of clouds may alleviate concerns and increase cloud usage; in consequence, it may decrease overall costs spent for the computational devices and infrastructures. This paper particu- larly focuses on the Platform-as-a-Service (PaaS) clouds. Security of PaaS clouds is considered from multiple perspectives including access control, privacy and service continuity while protecting both the service provider and the user. Security problems of PaaS clouds are explored and classified. Countermeasures are proposed and discussed. The achieved solutions are intended to be the rationales for future PaaS designs and implementations.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Scientific article, Cloud Computing:

   **Title**: Performance of Cloud Based Solutions. The Impact of Public Cloud, Private Cloud and Hybrid Cloud

   **Source**: https://www.grin.com/document/294956?lang=es
                                              
   **Author**: Mehmet Tahir Sandıkkaya and Ali Emre Harmancı 

   **Date**: Noviembre 2014

   **Summary**: The cloud paradigm introduces a change in visualization of system and data owned by an enterprise. Further, the on service-based sharing of resources such as storage, hardware and applications which are delivered with cloud computing in a total different way has facilitated coherence of the resources and economies of scale through its pay-per-use business model. It is no longer a collection of devices on a physical location and run a particular software program with all the needed data and resources present at a physical location but instead is a system which is geographically distributed with consideration on both application and data. But even when the development of distributed cloud architectures and services are all dealing with the same issues of scalability, elasticity over demand, broad network access, usage measurement, security aspects such as authorization and authentication, and many other concepts related to multitenant services in order to serve a high number of concurrent users over the internet, is it the main goal for companies to find the right solution for their requierments. The right solution can be a public, a private or a hybrid cloud and although the issues are very similar in any of these solutions, it depends further on the degree of potency of one or some issues which are related to different kind of industries and organisations to evaluate the right cloud based approach for a particular company. This means we have to agree that the evolution of a new paradigm requires adaptation in usage patterns and associated functional areas to fully benefit from the paradigm shift.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Scientific article, Cloud Computing:

   **Title**: Security Issues: Public vs Private vs Hybrid Cloud Computing 

   **Source**: http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.245.1453&rep=rep1&type=pdf
                                              
   **Author**: R.Balasubramanian and M.Aramudhan

   **Date**: Octubre 2012

   **Summary**:Cloud computing appears as a new paradigm and its main objective is to provide secure, quick, convenient data storage and net computing service. Even though cloud computing effectively reduces the cost and maintenance of IT industry security issues plays a very important role. More and more IT companies are shifting to cloud based service like Private, Public and Hybrid cloud computing. But at the same time they are concerned about security issues. In this paper much attention is given to Public, Private and Hybrid cloud computing issues., as more business today utilize cloud services and architectures more threats and concerns arise. An analysis of comparative benefits of different styles of cloud computing by using SPSS is also discussed here. 

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Scientific article, Cloud Computing:

   **Title**: Survey on Microservice Architecture - Security, Privacy and Standardization on Cloud Computing Environment  

   **Source**: http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.245.1453&rep=rep1&type=pdf
                                              
   **Author**: Washington Henrique Carvalho Almeida, Luciano de Aguiar Monteiro, Raphael Rodrigues Hazin, Anderson Cavalcanti de Lima and Felipe Silva Ferraz

   **Date**: Octubre 2017

   **Summary**:Microservices have been adopted as a natural solution for the replacement of monolithic systems. Some technologies and standards have been adopted for the development of microservices in the cloud environment; API and REST have been adopted on a large scale for their implementation. The purpose of the present work is to carry out a bibliographic survey on the microservice architecture focusing mainly on security, privacy and standardization aspects on cloud computing environments. This paper presents a bundle of elements that must be considered for the construction of solutions based on microservices.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Scientific article, Cloud Computing:

   **Title**: SMicroservices Architecture based Cloudware Deployment Platform for Service Computing  

   **Source**: https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7473049
                                              
   **Author**: Dong Guo, Wei Wang , Guosun Zeng, Zerong Wei

   **Date**: Octubre 2017

   **Summary**: With the rising of Cloud computing, evolution have occurred not only in datacenter, but also in software development, deployment, maintain and usage. How to build cloud platform for traditional software, and how to deliver cloud service to users are central research fields which will have a huge impact. In recent years, the development of microservice and container technology make software paradigm evolve towards Cloudware in cloud environment. Cloudware, which is based on service and supported by cloud platform, is an important method to cloudalize traditional software. It is also a significant way for software development, deployment, maintenance and usage in future cloud environment. Furthermore, it creates a completely new thought for software in cloud platform. In this paper, we proposed a new Cloudware PaaS platform based on microservice architecture and light weighted container technology. We can directly deploy traditional software which provides services to users by browser in this platform without any modification. By utilizing the microservice architecture, this platform has the characteristics of scalability, auto-deployment, disaster recovery and elastic configuration. 

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################


