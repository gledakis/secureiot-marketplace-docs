.. _GeneralBlogs:

Blogs
#####

.. topic:: Blogs, Cloud Computing:

   **Title**: Blog Cloud Computing  David Linthicum

   **Source**: https://www.infoworld.com/blog/cloud-computing/
                 
   **Author**: David Linthicum, Cloud Computing

   **Date**: 2015

   **Summary**: David Linthicum, the CTO and founder of Blue Mountain Labs, is widely recognized as a thought leader in the cloud computing industry -- and with good reason. He travels to deliver keynote speeches on cloud, has contributed to or authored 13 books, and writes the Cloud Computing blog. With provocative titles such as "Shocker: Government agency drafts sensible cloud computing strategy," "Wozniak is wrong about cloud computing" and "Wake up, IT: Even CFOs see value in the cloud," Linthicum isn't afraid to state his opinion -- and with his extensive cloud background, everyone should take note.Check out Linthicum's monthly column on SearchCloudComputing.com, in which he discusses topics that range from the cloud API wars to cloud portability and interoperability struggles.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

.. topic:: Blogs, Cloud Computing:

   **Title**: Cloud Tech

   **Source**: https://www.cloudcomputing-news.net
                 
   **Author**: Cloud Tech

   **Date**: 2016

   **Summary**: CloudTech is a leading blog and news site that is dedicated to cloud computing strategy and technology. With authors including IBM’s Sebastian Krause, Cloudonomics author Joe Weinman, and Ian Moyse from the Cloud Industry Forum, CloudTech has hundreds of blogs about numerous cloud-related topics and reaches over 320,000 cloud computing professionals. A recent post How the Financial Services Industry Is Slowly Waking Up to Cloud Computing by Rahul Singh of HCL Technologies provides an interesting analysis of how banks can overcome the barriers to cloud migration.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

.. topic:: Blogs, Cloud Computing:

   **Title**: The best page with the 25 best blogs in the world of cloud computing

   **Source**: https://www.cloudendure.com/blog/top-25-must-read-cloud-computing-blogs/
                 
   **Author**: Cloudendure

   **Date**: 2018

   **Summary**: In this page you will find the 25 most relevant blogs in the world about cloud computing.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Blogs, Cloud Computing:

   **Title**: Top five must-read cloud computing blogs

   **Source**: https://www.cloudendure.com/blog/top-25-must-read-cloud-computing-blogs/
                 
   **Author**: Werner Vogels, All Things Distributed, Randy Bias, Cloudscaling ,Adrian Cockcroft, Adrian Cockcroft's, Lori MacVittie, Two Different Socks, David Linthicum, Cloud Computing

   **Date**: 2015

   **Summary**: Werner Vogels, All Things Distributed: These top bloggers are in no particular order, but kicking off the list with the CTO of what's arguably cloud computing's king vendor, Amazon Web Services, should come as no surprise. Werner Vogels' blog, All Things Distributed, is not strictly about cloud computing, but it does give insight into AWS' strategy and vision, which should be of interest to any cloud pro. With some personality, regular updates and blogs jam-packed with information, Vogels proves he's one to beat in the blogging arena. Randy Bias, Cloudscaling blog: Randy Bias has a lengthy resume. Featured in publications as prestigious as Forbes, The Economist and The Wall Street Journal, Bias has a lot to say about cloud computing. This founding CEO and CTO of Cloudscaling also regularly contributes, along with other cloud experts and pioneers, to Cloudscaling's blog. With posts on Amazon's secret to success, the open source cloud market and the rise of DevOps, Bias focuses on what he calls "the profound disruption" cloud computing causes. If anyone can help IT navigate this disrupted landscape, Bias is at the top of the list. Adrian Cockcroft, Adrian Cockcroft's blog: Though simply titled, Adrian Cockcroft's blog has been covering the complex world of cloud architectures and performance tools -- as well as his interest in cars and music -- since 2004. He may not update the blog as often as Bias or Vogels, but his monthly (or so) insights are always worth a read. As a cloud architect at Netflix, Adrian Cockcroft is keen on discussing how his company handles the cloud computing evolution and how that compares to other industry leaders and trends, tackling subjects like Platform as a Service's (PaaS) place in the market and the use of popular AWS products.Lori MacVittie, Two Different Socks: Recognized as one of the Top Women in Cloud by CloudNOW, a nonprofit consortium featuring female leaders in cloud computing, Lori MacVittie is already well-known as a cloud leader. MacVittie's blog, Two Different Socks, delves into cloud performance struggles, DevOps and other vital issues in the cloud industry, with the help of charts, infographics and fun images. As the senior technical marketing manager at F5 Networks, MacVittie still had time to co-author a book called The Cloud Security Rules. David Linthicum, Cloud Computing: David Linthicum, the CTO and founder of Blue Mountain Labs, is widely recognized as a thought leader in the cloud computing industry -- and with good reason. He travels to deliver keynote speeches on cloud, has contributed to or authored 13 books, and writes the Cloud Computing blog. With provocative titles such as "Shocker: Government agency drafts sensible cloud computing strategy," "Wozniak is wrong about cloud computing" and "Wake up, IT: Even CFOs see value in the cloud," Linthicum isn't afraid to state his opinion -- and with his extensive cloud background, everyone should take note.Check out Linthicum's monthly column on SearchCloudComputing.com, in which he discusses topics that range from the cloud API wars to cloud portability and interoperability struggles.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################
