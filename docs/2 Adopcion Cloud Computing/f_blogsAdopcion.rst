.. _GeneralBlogsAdopcion:

Blogs
#######

.. topic:: Blogs, Cloud Computing Adoption:

   **Title**: 5 TIPS TO LEARN DOCKER IN 2018

   **Source**: https://blog.docker.com/2018/01/5-tips-learn-docker-2018/
                                              
   **Author**: Victor Coisne

   **Date**: January, 2018

   **Summary**: 5 TIPS TO LEARN DOCKER IN 2018

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

.. topic:: Blogs, Cloud Computing Adoption:

   **Title**: Play With Docker: the Docker Playground and Training site

   **Source**: https://training.play-with-docker.com
                                              
   **Author**: Victor Coisne

   **Date**: 2018

   **Summary**: Play with Docker (PWD) is a Docker playground and training site which allows users to run Docker commands in a matter of seconds. It gives the experience of having a free Linux Virtual Machine in browser, where you can build and run Docker containers and even create clusters. Check out this video from DockerCon 2017 to learn more about this project. The training site is composed of a large set of Docker labs and quizzes from beginner to advanced level available for both Developers and IT pros at  training.play-with-docker.com.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

.. topic:: Blogs, Cloud Computing Adoption:

   **Title**: DockerCon 2018

   **Source**: https://2018.dockercon.com
                                              
   **Author**: DockerCon

   **Date**: 2018

   **Summary**: In case you missed it, DockerCon 2018 will take place at Moscone Center, San Francisco, CA on June 13-15, 2018. DockerCon is where the container community comes to learn, belong, and collaborate. Attendees are a mix of beginner, intermediate, and advanced users who are all looking to level up their skills and go home inspired. With a 2 full days of training, more than 100 sessions, free workshops and hands-on labs and the wealth of experience brought by each attendee, DockerCon is the place to be if you’re looking to learn Docker in 2018.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

.. topic:: Blogs, Cloud Computing Adoption:

   **Title**: Docker Meetups

   **Source**: https://www.meetup.com/es-ES/Docker-Online-Meetup/?chapter_analytics_code=UA-48368587-1
                                              
   **Author**: Docker Meetups

   **Date**: 2018

   **Summary**: Look at our Docker Meetup Chapters page to see if there is a Docker user group in your city. With more than 200 local chapters in 81 countries, you should be able to find one near you! Attending local Docker meetups are an excellent way to learn Docker. The community leaders who run the user group often schedule Docker 101 talks and hands-on training for newcomers!

Can’t find a chapter near you? Join the Docker Online meetup group to attend meetups remotely!

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

.. topic:: Blogs, Cloud Computing Adoption:

   **Title**: Docker Captains

   **Source**: https://www.docker.com/community/captains
                                              
   **Author**: Docker Captains

   **Date**: 2018

   **Summary**: Captains are Docker experts that are leaders in their communities, organizations or ecosystems. As Docker advocates, they are committed to sharing their knowledge and do so every chance they get! Captains are advisors, ambassadors, coders, contributors, creators, tool builders, speakers, mentors, maintainers and super users and are required to be active stewards of Docker in order to remain in the program.

Follow all of the Captains on twitter. Also check out the Captains GitHub repo to see what projects they have been working on. Docker Captains are eager to bring their technical expertise to new audiences both offline and online around the world – don’t hesitate to reach out to them via the social links on their Captain profile pages. You can filter the captains by location, expertise, and more.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

.. topic:: Blogs, Cloud Computing Adoption:

   **Title**: Training and Certification

   **Source**: https://europe-2017.dockercon.com
                                              
   **Author**: DockerCon

   **Date**: 2018

   **Summary**: The new Docker Certified Associate (DCA) certification, launching at DockerCon Europe on October 16, 2017, serves as a foundational benchmark for real-world container technology expertise with Docker Enterprise Edition. In today’s job market, container technology skills are highly sought after and this certification sets the bar for well-qualified professionals. The professionals that earn the certification will set themselves apart as uniquely qualified to run enterprise workloads at scale with Docker Enterprise Edition and be able to display the certification logo on resumes and social media profiles.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

.. topic:: Blogs, Cloud Computing Adoption:

   **Title**: Kubernetes Oficial Blog

   **Source**: https://kubernetes.io/blog/
                                              
   **Author**: Kubernetes

   **Date**: 2017

   **Summary**: Kubernetes Blog

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

.. topic:: Blogs, Cloud Computing Adoption:

   **Title**: Blog Oficial Eclipse Che

   **Source**: https://che.eclipse.org/what-is-eclipse-che-815d110f64e5
                                              
   **Author**: Eclipse Che

   **Date**: 2016

   **Summary**: Eclipse Che Blog

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################




