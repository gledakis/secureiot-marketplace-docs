.. _GeneralExamplesAdopcion:

Examples
########

.. topic:: Examples, Cloud Computing Adoption:

   **Title**: Create first images in docker

   **Source**: https://docs.docker.com/develop/develop-images/baseimages/
                                              
   **Author**:

   **Date**: 

   **Summary**: 

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Examples, Cloud Computing Adoption:

   **Title**: Docker Series — Building your first image:

   **Source**: https://medium.com/pintail-labs/docker-series-building-your-first-image-8a6f051ae637      
                                        
   **Author**:

   **Date**: 

   **Summary**: 

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################
