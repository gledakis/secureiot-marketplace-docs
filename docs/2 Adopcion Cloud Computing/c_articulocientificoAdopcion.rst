.. _GeneralArticulosCAdopcion:

Scientific articles
###################

.. topic:: Scientific article, Cloud Computing Adoption:

   **Title**: Model-Driven Management of Docker Containers

   **Source**: https://ieeexplore.ieee.org/document/7820337/ 
                                             
   **Author**: Fawaz Paraiso ; Stéphanie Challita ; Yahya Al-Dhuraibi ; Philippe Merle

   **Date**: 

   **Summary**: With the emergence of Docker, it becomes easier to encapsulate applications and their dependencies into lightweight Linux containers and make them available to the world by deploying them in the cloud. Compared to hypervisor-based virtualization approaches, the use of containers provides faster start-ups times and reduces the consumption of computer resources. However, Docker lacks of deployability verification tool for containers at design time. Currently, the only way to be sure that the designed containers will execute well is to test them in a running system. If errors occur, a correction is made but this operation can be repeated several times before the deployment becomes operational. Docker does not provide a solution to increase or decrease the size of container resources in demand. Besides the deployment of containers, Docker lacks of synchronization between the designed containers and those deployed. Moreover, container management with Docker is done at low level, and therefore requires users to focus on low level system issues. In this paper we focus on these issues related to the management of Docker containers. In particular, we propose an approach for modeling Docker containers. We provide tooling to ensure the deployability and the management of Docker containers. We illustrate our proposal using an event processing application and show how our solution provides a significantly better compromise between performance and development costs than the basic Docker container solution.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Scientific article, Cloud Computing Adoption:

   **Title**: Docker Reference Architecture: Designing Scalable, Portable Docker Container Networks

   **Source**: https://success.docker.com/article/networking
                                              
   **Author**: Docker 

   **Date**: 2018

   **Summary**: Docker containers wrap a piece of software in a complete filesystem that contains everything needed to run: code, runtime, system tools, system libraries – anything that can be installed on a server. This guarantees that the software will always run the same, regardless of its environment. By default, containers isolate applications from one another and the underlying infrastructure, while providing an added layer of protection for the application.

What if the applications need to communicate with each other, the host, or an external network? How do you design a network to allow for proper connectivity while maintaining application portability, service discovery, load balancing, security, performance, and scalability? This document addresses these network design challenges as well as the tools available and common deployment patterns. It does not specify or recommend physical network design but provides options for how to design Docker networks while considering the constraints of the application and the physical network.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Scientific article, Cloud Computing Adoption:

   **Title**: Docker Commands — The Ultimate Cheat Sheet

   **Source**: https://hackernoon.com/docker-commands-the-ultimate-cheat-sheet-994ac78e2888
                                              
   **Author**: Nick Parsons

   **Date**: Aug 2017

   **Summary**: If you don’t already know, Docker is an open-source platform for building distributed software using “containerization,” which packages applications together with their environments to make them more portable and easier to deploy.Thanks to its power and productivity, Docker has become an incredibly popular technology for software development teams. However, this same power can sometimes make it tricky for new users to jump into the Docker ecosystem, or even for experienced users to remember the right command. Fortunately, with the right learning tools you’ll have a much easier time getting started with Docker. This article will be your one-stop shop for Docker, going over some of the best practices and must-know commands that any user should know.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

.. topic:: Scientific article, Cloud Computing Adoption:

   **Title**: Features Eclipse Che

   **Source**: https://www.eclipse.org/che/features/
                                              
   **Author**: Eclipse Che

   **Date**: October 2017

   **Summary**: Eclipse Che is a developer workspace server and cloud IDE built for teams and organizations

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################
