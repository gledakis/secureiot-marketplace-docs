####################################
Adoption of Cloud Computing paradigm
####################################

*Potential adopters of Cloud Computing technology perceive the adoption decision as very challenging. Cloud Computing adoption should be driven by change management, competence, maturity, among others. Cloud Computing is still in its stage of emergence, and there is still a lack of both knowledge and empirical evidence about which issues are the most significant for Cloud Computing adoption decisions. The following resources have been selected to give some light to these issues.*

=================
Magazine articles
=================

   **Title**: What is Docker and why is it so darn popular?

   **Source**: https://www.zdnet.com/article/what-is-docker-and-why-is-it-so-darn-popular/
                                              
   **Author**: Steven J. Vaughan-Nichols

   **Date**: Nov 2016

   **Summary**: Docker is hotter than hot because it makes it possible to get far more apps running on the same old servers and it also makes it very easy to package and ship programs. Here's what you need to know about it.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

   **Title**: A Beginner’s Guide to Kubernete:

   **Source**: https://medium.com/containermind/a-beginners-guide-to-kubernetes-7e8ca56420b6
                                              
   **Author**: Imesh Gunaratne

   **Date**: Aug 2017

   **Summary**: Kubernetes has now become the de facto standard for deploying containerized applications at scale in private, public and hybrid cloud environments. The largest public cloud platforms AWS, Google Cloud, Azure, IBM Cloud and Oracle Cloud now provide managed services for Kubernetes. A few years back RedHat completely replaced their OpenShift implementation with Kubernetes and collaborated with the Kubernetes community for implementing the next generation container platform. Mesosphere incorporated key features of Kubernetes such as container grouping, overlay networking, layer 4 routing, secrets, etc into their container platform DC/OS soon after Kubernetes got popular. DC/OS also integrated Kubernetes as a container orchestrator alongside Marathon. Pivotal recently introduced Pivotal Container Service (PKS) based on Kubernetes for deploying third-party services on Pivotal Cloud Foundry and as of today there are many other organizations and technology providers adapting it at a rapid phase.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: What is Kubernetes? Container orchestration explained

   **Source**: https://www.infoworld.com/article/3268073/kubernetes/what-is-kubernetes-container-orchestration-explained.html
                                              
   **Author**: Serdar Yegulalp

   **Date**: Aug 2017

   **Summary**: The rise of containers has reshaped the way people think about developing, deploying, and maintaining software. Drawing on the native isolation capabilities of modern operating systems, containers support VM-like separation of concerns, but with far less overhead and far greater flexibility of deployment than hypervisor-based virtual machines.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

===================
Scientific articles
===================

   **Title**: Model-Driven Management of Docker Containers

   **Source**: https://ieeexplore.ieee.org/document/7820337/ 
                                             
   **Author**: Fawaz Paraiso ; Stéphanie Challita ; Yahya Al-Dhuraibi ; Philippe Merle

   **Date**: Aug 2017

   **Summary**: With the emergence of Docker, it becomes easier to encapsulate applications and their dependencies into lightweight Linux containers and make them available to the world by deploying them in the cloud. Compared to hypervisor-based virtualization approaches, the use of containers provides faster start-ups times and reduces the consumption of computer resources. However, Docker lacks of deployability verification tool for containers at design time. Currently, the only way to be sure that the designed containers will execute well is to test them in a running system. If errors occur, a correction is made but this operation can be repeated several times before the deployment becomes operational. Docker does not provide a solution to increase or decrease the size of container resources in demand. Besides the deployment of containers, Docker lacks of synchronization between the designed containers and those deployed. Moreover, container management with Docker is done at low level, and therefore requires users to focus on low level system issues. In this paper we focus on these issues related to the management of Docker containers. In particular, we propose an approach for modeling Docker containers. We provide tooling to ensure the deployability and the management of Docker containers. We illustrate our proposal using an event processing application and show how our solution provides a significantly better compromise between performance and development costs than the basic Docker container solution.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: Docker Reference Architecture: Designing Scalable, Portable Docker Container Networks

   **Source**: https://success.docker.com/article/networking
                                              
   **Author**: Docker 

   **Date**: Aug 2018

   **Summary**: Docker containers wrap a piece of software in a complete filesystem that contains everything needed to run: code, runtime, system tools, system libraries – anything that can be installed on a server. This guarantees that the software will always run the same, regardless of its environment. By default, containers isolate applications from one another and the underlying infrastructure, while providing an added layer of protection for the application.

What if the applications need to communicate with each other, the host, or an external network? How do you design a network to allow for proper connectivity while maintaining application portability, service discovery, load balancing, security, performance, and scalability? This document addresses these network design challenges as well as the tools available and common deployment patterns. It does not specify or recommend physical network design but provides options for how to design Docker networks while considering the constraints of the application and the physical network.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: Docker Commands — The Ultimate Cheat Sheet

   **Source**: https://hackernoon.com/docker-commands-the-ultimate-cheat-sheet-994ac78e2888
                                              
   **Author**: Nick Parsons

   **Date**: Aug 2017

   **Summary**: If you don’t already know, Docker is an open-source platform for building distributed software using “containerization,” which packages applications together with their environments to make them more portable and easier to deploy.Thanks to its power and productivity, Docker has become an incredibly popular technology for software development teams. However, this same power can sometimes make it tricky for new users to jump into the Docker ecosystem, or even for experienced users to remember the right command. Fortunately, with the right learning tools you’ll have a much easier time getting started with Docker. This article will be your one-stop shop for Docker, going over some of the best practices and must-know commands that any user should know.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

   **Title**: Features Eclipse Che

   **Source**: https://www.eclipse.org/che/features/
                                              
   **Author**: Eclipse Che

   **Date**: October 2017

   **Summary**: Eclipse Che is a developer workspace server and cloud IDE built for teams and organizations

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

   **Title**: Understanding cloud computing adoption issues: A Delphi study approach

   **Source**: https://www.sciencedirect.com/science/article/pii/S016412121630036X
                                              
   **Author**: Science Direct

   **Date**: Aug 2016

   **Summary**: This research paper reports on a Delphi study focusing on the most important issues enterprises are confronted with when making cloud computing (CC) adoption decisions. We had 34 experts from different domain backgrounds participated in a Delphi panel. The panelists were IT and CC specialists representing a heterogeneous group of clients, providers and academics, divided into three subpanels. The Delphi procedure comprised three stages: brainstorming, narrowing down and ranking. The panelists identified 55 issues of concerns in the first stage, which were analyzed and grouped into 10 categories: security, strategy, legal and ethical, IT governance, migration, culture, business, awareness, availability and impact. The top 18 issues for each subpanel were ranked, and a moderate intrapanel consensus was obtained. Additionally, 16 follow-up interviews were conducted with the experts to get a deeper understanding of the issues and why certain issues were more significant than others. The findings indicate that security, strategy and legal and ethical issues are the most important. The discussion resulted in highlighting certain inhibitors and drivers for CC adoption into a framework. The paper is concluded with key recommendations with focus on change management, competence and maturity to inform decision-makers in CC adoption decisions.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

=========
Tutorials
=========

   **Title**: 47 advanced tutorials for mastering Kubernetes:

   **Source**: https://techbeacon.com/top-tutorials-mastering-kubernetes
                                              
   **Author**: Tech Beacon

   **Date**: Oct 2017

   **Summary**: Best tutorials for mastering Kubernetes

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

   **Title**: Top Tutorials To Learn Kubernetes

   **Source**: https://medium.com/quick-code/top-tutorials-to-learn-kubernetes-e9507e76d9a4
                                              
   **Author**: Medium Blog

   **Date**: Nov 2017

   **Summary**: Learn Kubernetes

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

   **Title**: Kubernetes cluster architecture:

   **Source**: https://www.coursera.org/lecture/deploy-micro-kube-ibm-cloud/kubernetes-cluster-architecture-9DY7Z
                                              
   **Author**: Coursera, IBM

   **Date**: April 2017

   **Summary**:  First Kubernetes cluster architecture

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

   **Title**: Create first images in docker

   **Source**: https://docs.docker.com/develop/develop-images/baseimages/
                                              
   **Author**: Docker

   **Date**: Aug 2016

   **Summary**: Create images in docker

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

   **Title**: Docker Series — Building your first image:

   **Source**: https://medium.com/pintail-labs/docker-series-building-your-first-image-8a6f051ae637      
                                        
   **Author**: Docker

   **Date**: Nov 2016

   **Summary**: The Docker image we are going to build is the image used to start the pintail-whoami container we used in the article Docker Series — Starting your first container.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

   **Title**: Network Configuration

   **Source**: https://docs.docker.com/v1.5/articles/networking/   
                                        
   **Author**: Docker

   **Date**: Aug 2016

   **Summary**: When Docker starts, it creates a virtual interface named docker0 on the host machine. It randomly chooses an address and subnet from the private range defined by RFC 1918 that are not in use on the host machine, and assigns it to docker0. Docker made the choice 172.17.42.1/16 when I started it a few minutes ago, for example — a 16-bit netmask providing 65,534 addresses for the host machine and its containers. The MAC address is generated using the IP address allocated to the container to avoid ARP collisions, using a range from 02:42:ac:11:00:00 to 02:42:ac:11:ff:ff...

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

=======
Courses
=======

   **Title**: DOCKER FUNDAMENTALS + ENTERPRISE DEVELOPERS COURSE BUNDLE

   **Source**: https://training.docker.com/instructor-led-training/docker-fundamentals-enterprise-developers
                                              
   **Author**: Docker

   **Date**: 2017

   **Summary**: As the follow-on to the Docker Fundamentals course, Docker for Enterprise Operations is a role-based course designed for an organization’s Development and DevOps teams to accelerate their Docker journey in the enterprise. The course covers in-depth core advanced features of Docker EE and best practices to apply these features at scale with enterprise workloads. It is highly recommended to complete the Docker Fundamentals course as a pre-requisite.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

   **Title**: General docker courses

   **Source**: https://training.docker.com/instructor-led-training
                                              
   **Author**: Docker

   **Date**: 2017

   **Summary**: Docker Fundamentals: (2 days, $1495) - https://success.docker.com/training/courses/docker-fundamentals

This is THE introductory Docker course to give your team the best foundation for enterprise grade Docker use-cases.

Docker for Enterprise Developers: (2 days, $1995) - https://success.docker.com/training/courses/docker-for-enterprise-developers

As the follow-on to our Docker Fundamentals course, Docker for Enterprise Developers is a role-based course designed for an organization’s Development and DevOps teams to accelerate their Docker journey in the enterprise.

Docker for Enterprise Operations:(2 Days, $1995) - https://success.docker.com/trainingcourses/docker-for-enterprise-operations

This course is the second level of Docker's core curriculum for the enterprise and is focused on the Docker Operator role in administration of Docker Enterprise Edition Advanced.

Docker Troubleshooting & Support : (2 Days, $1995) - https://success.docker.com/training/courses/docker-support-troubleshooting

The Docker Troubleshooting & Support course is a role-based course designed for an organization’s support teams to troubleshoot the variety of issues that arise in their Docker journey.

Docker Security: (1 Day, $995) - https://success.docker.com/training/courses/docker-security

This is the Docker Security course for your entire organization. Get everyone “on-the-same page” and working together to secure your Dockerized environment. This hands-on workshop style course will give your team an overview of important security features and best practices to protect your containerized services.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

=====
Blogs
=====

   **Title**: 5 TIPS TO LEARN DOCKER IN 2018

   **Source**: https://blog.docker.com/2018/01/5-tips-learn-docker-2018/
                                              
   **Author**: Victor Coisne

   **Date**: January, 2018

   **Summary**: 5 TIPS TO LEARN DOCKER IN 2018

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

   **Title**: Play With Docker: the Docker Playground and Training site

   **Source**: https://training.play-with-docker.com
                                              
   **Author**: Victor Coisne

   **Date**: 2018

   **Summary**: Play with Docker (PWD) is a Docker playground and training site which allows users to run Docker commands in a matter of seconds. It gives the experience of having a free Linux Virtual Machine in browser, where you can build and run Docker containers and even create clusters. Check out this video from DockerCon 2017 to learn more about this project. The training site is composed of a large set of Docker labs and quizzes from beginner to advanced level available for both Developers and IT pros at  training.play-with-docker.com.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

   **Title**: DockerCon 2018

   **Source**: https://2018.dockercon.com
                                              
   **Author**: DockerCon

   **Date**: 2018

   **Summary**: In case you missed it, DockerCon 2018 will take place at Moscone Center, San Francisco, CA on June 13-15, 2018. DockerCon is where the container community comes to learn, belong, and collaborate. Attendees are a mix of beginner, intermediate, and advanced users who are all looking to level up their skills and go home inspired. With a 2 full days of training, more than 100 sessions, free workshops and hands-on labs and the wealth of experience brought by each attendee, DockerCon is the place to be if you’re looking to learn Docker in 2018.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

   **Title**: Docker Meetups

   **Source**: https://www.meetup.com/es-ES/Docker-Online-Meetup/?chapter_analytics_code=UA-48368587-1
                                              
   **Author**: Docker Meetups

   **Date**: 2018

   **Summary**: Look at our Docker Meetup Chapters page to see if there is a Docker user group in your city. With more than 200 local chapters in 81 countries, you should be able to find one near you! Attending local Docker meetups are an excellent way to learn Docker. The community leaders who run the user group often schedule Docker 101 talks and hands-on training for newcomers!

Can’t find a chapter near you? Join the Docker Online meetup group to attend meetups remotely!

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

   **Title**: Docker Captains

   **Source**: https://www.docker.com/community/captains
                                              
   **Author**: Docker Captains

   **Date**: 2018

   **Summary**: Captains are Docker experts that are leaders in their communities, organizations or ecosystems. As Docker advocates, they are committed to sharing their knowledge and do so every chance they get! Captains are advisors, ambassadors, coders, contributors, creators, tool builders, speakers, mentors, maintainers and super users and are required to be active stewards of Docker in order to remain in the program.

Follow all of the Captains on twitter. Also check out the Captains GitHub repo to see what projects they have been working on. Docker Captains are eager to bring their technical expertise to new audiences both offline and online around the world – don’t hesitate to reach out to them via the social links on their Captain profile pages. You can filter the captains by location, expertise, and more.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

   **Title**: Training and Certification

   **Source**: https://europe-2017.dockercon.com
                                              
   **Author**: DockerCon

   **Date**: 2018

   **Summary**: The new Docker Certified Associate (DCA) certification, launching at DockerCon Europe on October 16, 2017, serves as a foundational benchmark for real-world container technology expertise with Docker Enterprise Edition. In today’s job market, container technology skills are highly sought after and this certification sets the bar for well-qualified professionals. The professionals that earn the certification will set themselves apart as uniquely qualified to run enterprise workloads at scale with Docker Enterprise Edition and be able to display the certification logo on resumes and social media profiles.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

   **Title**: Kubernetes Oficial Blog

   **Source**: https://kubernetes.io/blog/
                                              
   **Author**: Kubernetes

   **Date**: 2017

   **Summary**: Kubernetes Blog

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

   **Title**: Blog Oficial Eclipse Che

   **Source**: https://che.eclipse.org/what-is-eclipse-che-815d110f64e5
                                              
   **Author**: Eclipse Che

   **Date**: 2016

   **Summary**: Eclipse Che Blog

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################
