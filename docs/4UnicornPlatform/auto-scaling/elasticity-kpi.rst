##########################
Overprovisioning Cost KPI
##########################

Overprovisioning Cost is the cost for reserving additional resources per unit of time to sastify unrealised demand.
To demonstrate the importance of the Overprovisioning Cost KPI let us consider an example of an e-commerce
company that uses cloud infrastructure to offer its services.

For this, the company requests from its cloud provider to reserve additional
idle resources to satisfy unforeseen demand in order to keep the application's performance at the desired levels.
These idle resources cause unnecessary cost which can be eliminated using the elasticity features
of the UNICORN platform.

.. topic:: How to measure?

   To measure the over-provisioning, one has to know for a given period of time
   the actual resources needed (red line) and also the available resource capacity (black line).
   The area between these lines gives the over-provisioning cost.

.. figure:: elasticity-kpi.png
   :scale: 75 %
   :alt: Over-provisioning Cost
   :align: center

   Over-provisioning cost
