###############################################
#####################################
Unicorn Platfrom - Architecture
###############################################

.. note::
  This section is still under work


========
Overview
========


.. note::
  This section is still under work



==========
Components
==========
* **Monitoring Agents**: lightweight entities deployable on any cloud element to be monitored responsible for coordinating and managing the metric collection process on the respective cloud element (e.g., container, VM), which includes aggregation and dissemination of monitoring data to the Monitoring Service over a secure control plane.
* **Monitoring Probes**: the actual metric collectors that adhere to a common metric collection interface with specific Monitoring Probe implementations gathering metrics from the underlying infrastructure.
* **Monitoring Library**: the source code annotation design library supporting application instrumentation for Unicorn compliant cloud applications.
* **Monitoring Service**: the entity easing the management of the monitoring infrastructure by providing scalable and multi-tenant monitoring alongside the Unicorn platform.
* **Monitoring Data Store**: a distributed and scalable data store with a high-performance indexing scheme for storing and extracting monitoring updates.
* **Monitoring REST API**: the entity responsible for managing and authorizing access to monitoring data stored in the Monitoring Data Store.


.. note::
  This section is still under work


============
Architecture
============

.. note::
  This section is still under work

.. image:: monarch.png
  :width: 100%
  :alt: Alternative text
  :align: center

